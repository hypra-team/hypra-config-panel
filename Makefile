#!/usr/bin/make -f

PACKAGE = hypra-config-panel
VERSION = 0.1
LINGUAS = fr

MODULE_SOURCES = \
	hcp/__init__.py \
	hcp/__main__.py \
	hcp/application.py \
	hcp/config.py \
	hcp/module.py \
	hcp/widgets/__init__.py \
	hcp/widgets/frame.py \
	hcp/widgets/labeledswitch.py \
	hcp/widgets/stackswitchernotebook.py \
	hcp/window.py \
	hcp/modules/__init__.py \
	hcp/modules/simple.py \
	$(null)

pkglibexec_SCRIPTS = \
	helpers/modules/simple/helpers/autostart.py \
	helpers/modules/simple/helpers/compiz-setting.py \
	helpers/modules/simple/helpers/mate-theme.py \
	helpers/modules/simple/helpers/mouse-localize.sh \
	helpers/modules/simple/helpers/orca-enabled.sh \
	helpers/modules/simple/helpers/orca-setting.py \
	helpers/modules/simple/helpers/visual-default-zoom.sh \
	helpers/modules/simple/helpers/visual-font-family.py \
	helpers/modules/simple/helpers/visual-font-sizes.sh \
	helpers/modules/simple/helpers/visual-invert-colors.sh \
	$(null)

pkgdata_DATA = \
	data/categories.json \
	data/modules/simple/options/mouse-010-localize.json \
	data/modules/simple/options/mouse-020-click-type.json \
	data/modules/simple/options/mouse-030-theme-color.json \
	data/modules/simple/options/mouse-040-theme-size.json \
	data/modules/simple/options/mouse-mouse-010-speed.json \
	data/modules/simple/options/mouse-touchpad-010-enable.json \
	data/modules/simple/options/mouse-touchpad-020-speed.json \
	data/modules/simple/options/visual-font-010-family.json \
	data/modules/simple/options/visual-font-020-size.json \
	data/modules/simple/options/visual-theme-020-invert-colors.json \
	data/modules/simple/options/visual-zoom-010-zoom.json \
	data/modules/simple/options/vocal-010-orca-enabled.json \
	data/modules/simple/options/vocal-orca-010-profile.json \
	data/modules/simple/options/vocal-orca-020-key-echo.json \
	data/modules/simple/options/vocal-orca-030-mouse-review.json \
	data/modules/simple/options/vocal-orca-040-voice-rate.json \
	data/modules/simple/options/vocal-orca-050-voice.json \
	data/window.css \
	data/window.ui \
	data/window-bg.svg \
	$(null)

pkgdoc_DATA = \
	doc/README.txt

SOURCES = \
	$(MODULE_SOURCES) \
	$(pkglibexec_SCRIPTS) \
	$(pkgdata_DATA) \
	$(PACKAGE) \
	$(PACKAGE)-run \
	$(PACKAGE).desktop.in \
	$(null)

ICONS = \
	data/icons/16x16/hypra-config-panel.png \
	data/icons/16x16/hypra-config-panel-symbolic.symbolic.png \
	data/icons/22x22/hypra-config-panel.png \
	data/icons/22x22/hypra-config-panel-symbolic.symbolic.png \
	data/icons/24x24/hypra-config-panel.png \
	data/icons/24x24/hypra-config-panel-symbolic.symbolic.png \
	data/icons/32x32/hypra-config-panel.png \
	data/icons/32x32/hypra-config-panel-symbolic.symbolic.png \
	data/icons/48x48/hypra-config-panel.png \
	data/icons/48x48/hypra-config-panel-symbolic.symbolic.png \
	data/icons/64x64/hypra-config-panel.png \
	data/icons/128x128/hypra-config-panel.png \
	data/icons/256x256/hypra-config-panel.png \
	data/icons/256x256/hypra-config-panel-symbolic.symbolic.png \
	$(null)

PREFIX ?= /usr
BINDIR ?= $(PREFIX)/bin
DATADIR ?= $(PREFIX)/share
APPLICATIONSDIR ?= $(DATADIR)/applications
DOCDIR ?= $(DATADIR)/doc
LOCALEDIR ?= $(DATADIR)/locale
ICONSDIR ?= $(DATADIR)/icons
LIBEXECDIR ?= $(PREFIX)/libexec
PYTHONDIR ?= $(PREFIX)/lib/python3/dist-packages
PKGDATADIR ?= $(DATADIR)/$(PACKAGE)
PKGDOCDIR ?= $(DOCDIR)/$(PACKAGE)
PKGLIBEXECDIR ?= $(LIBEXECDIR)/$(PACKAGE)

XGETTEXT_SOURCES = $(filter %.py %.sh %.ui %.desktop.in,$(SOURCES:.json=.json.py))

PODIR = po
POTFILE = $(PODIR)/$(PACKAGE).pot

INSTALL ?= install

XGETTEXT ?= xgettext
_XGETTEXT_FLAGS := -kpgettext:1c,2 --from-code=UTF-8 -cTranslators: \
		   --package-name=$(PACKAGE) --package-version=$(VERSION)
MSGMERGE ?= msgmerge
_MSGMERGE_FLAGS := --update
MSGINIT ?= msginit
_MSGINIT_FLAGS :=
MSGFMT ?= msgfmt
_MSGFMT_FLAGS := --check

PYTHON3 ?= python3
PY_COMPILE ?= $(PYTHON3) -m py_compile


all:

update-po: $(POTFILE) $(LINGUAS:%=po/%.po)

clean:
	$(RM) $(filter %.json.py,$(XGETTEXT_SOURCES))
	$(RM) $(LINGUAS:%=$(PODIR)/%.mo)
	$(RM) $(PODIR)/LINGUAS

%.po: $(POTFILE) Makefile
	if test -f "$@"; then \
		$(MSGMERGE) $(_MSGMERGE_FLAGS) $(MSGMERGE_FLAGS) $@ $< && touch "$@"; \
	else \
		$(MSGINIT) $(_MSGINIT_FLAGS) $(MSGINIT_FLAGS) -i $< -o $@ -l $(@:$(PODIR)/%.po=%); \
	fi

%.mo: %.po Makefile
	$(MSGFMT) $(_MSGFMT_FLAGS) $(MSGFMT_FLAGS) -o $@ $<

$(PODIR)/LINGUAS: Makefile
	echo $(LINGUAS) > $@

$(PACKAGE).desktop: $(PACKAGE).desktop.in $(LINGUAS:%=$(PODIR)/%.po) $(PODIR)/LINGUAS
	$(MSGFMT) $(_MSGFMT_FLAGS) $(MSGFMT_FLAGS) --desktop --template $< -d $(PODIR) -o $@

%.json.py: %.json json2pypo.py Makefile
	$(PYTHON3) json2pypo.py $< >$@

$(POTFILE): $(XGETTEXT_SOURCES) Makefile
	$(XGETTEXT) $(_XGETTEXT_FLAGS) $(XGETTEXT_FLAGS) -o $@ -- $(XGETTEXT_SOURCES)

install: $(PACKAGE).desktop
	$(INSTALL) -D -m 755 -t "$(DESTDIR)/$(LIBEXECDIR)" "$(PACKAGE)-run"
	$(INSTALL) -D -m 755 -t "$(DESTDIR)/$(BINDIR)" "$(PACKAGE)"
	$(INSTALL) -D -m 644 -t "$(DESTDIR)/$(APPLICATIONSDIR)" "$(PACKAGE).desktop"

install: install-module
install-module: $(MODULE_SOURCES)
	for f in $^; do \
		d="$(DESTDIR)/$(PYTHONDIR)/$${f%/*}"; \
		$(INSTALL) -d "$$d" || exit; \
		$(INSTALL) -m 644 -t "$$d" "$$f" || exit; \
		$(PY_COMPILE) "$$d/$${f##*/}"; \
	done

install: install-libexec
install-libexec: $(pkglibexec_SCRIPTS)
	for f in $^; do \
		d="/$${f#*/}"; d="$(DESTDIR)/$(PKGLIBEXECDIR)/$${d%/*}"; \
		$(INSTALL) -d "$$d" || exit; \
		$(INSTALL) -m 755 -t "$$d" "$$f" || exit; \
	done

install: install-data
install-data: $(pkgdata_DATA)
	for f in $^; do \
		d="/$${f#*/}"; d="$(DESTDIR)/$(PKGDATADIR)/$${d%/*}"; \
		$(INSTALL) -d "$$d" || exit; \
		$(INSTALL) -m 644 -t "$$d" "$$f" || exit; \
	done

install: install-i18n
install-i18n: $(LINGUAS:%=$(PODIR)/%.mo) Makefile
	for ll in $(LINGUAS); do \
		$(INSTALL) -D -m 0644 $(PODIR)/$$ll.mo $(DESTDIR)/$(LOCALEDIR)/$$ll/LC_MESSAGES/$(PACKAGE).mo || exit; \
	done

install: install-icons
install-icons: $(ICONS)
	for f in $^; do \
		d="$${f##data/icons/}" ; \
		d="$(DESTDIR)/$(ICONSDIR)/hicolor/$${d%/*}/apps" ; \
		$(INSTALL) -d "$$d" || exit; \
		$(INSTALL) -m 644 -t "$$d" "$$f" || exit; \
	done

install: install-doc
install-doc: $(pkgdoc_DATA)
	for f in $^; do \
		d="/$${f#*/}"; d="$(DESTDIR)/$(PKGDOCDIR)/$${d%/*}"; \
		$(INSTALL) -d "$$d" || exit; \
		$(INSTALL) -m 644 -t "$$d" "$$f" || exit; \
	done

rdist: $(PACKAGE)-$(VERSION).tgz
.PHONY: $(PACKAGE)-$(VERSION).tgz
$(PACKAGE)-$(VERSION).tgz:
	tmp="$$(mktemp -d)"; \
	if $(MAKE) PY_COMPILE=true DESTDIR="$$tmp" PREFIX=/usr install; then \
		old="$$PWD"; \
		cd "$$tmp" && \
		tar -c -z -v -f "$$old/$@" --transform='s/^usr/$(PACKAGE)-$(VERSION)/' usr; \
	fi; \
	$(RM) -r "$$tmp"

check: check-json check-po

check-po:
	$(MSGFMT) --check --statistics $(LINGUAS:%=po/%.po)

check-json: check-json-invalidates

check-json-invalidates:
	for json in $(filter %.json,$(SOURCES)); do \
		$(PYTHON3) -c 'import sys, os, glob, json; sys.exit(0 if all(glob.glob(os.path.join(os.path.dirname(sys.argv[1]), pat if pat.endswith(".json") else pat + ".json")) for pat in json.load(open(sys.argv[1])).get("invalidates", [])) else 1)' "$$json" || echo "WARNING: $$json contains invalidating patterns with no matches" >&2; \
	done

.PHONY: all check install update-po

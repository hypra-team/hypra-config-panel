#!/usr/bin/env python3
# -*- coding: utf-8; -*-
#
#  Helper script to enable or disable an autostart application.
#
#  Copyright (C) 2021  Colomban Wendling <cwendling@hypra.fr>
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.

import sys
import os
import shlex
import configparser
import logging
import re


def get_user_autostart_path():
    config_home = os.environ.get('XDG_CONFIG_HOME', os.path.expanduser('~/.config'))
    return os.path.join(config_home, 'autostart')


def autostart_paths():
    yield get_user_autostart_path()
    config_dirs = os.environ.get('XDG_CONFIG_DIRS', '/etc/xdg')
    for path in config_dirs.split(':'):
        yield os.path.join(path, 'autostart')


class DesktopEntry(configparser.ConfigParser):
    SECTION = 'Desktop Entry'

    def __init__(self):
        super().__init__(interpolation=None, strict=False)

    def optionxform(self, option):
        """ Keep case in option names """
        return option

    def _check(self):
        """ Checks if the entry seems valid """
        if not self.has_section(self.SECTION) or \
           not self.has_option(self.SECTION, 'Type'):
            raise configparser.NoSectionError('Invalid Desktop Entry file')

    def read(self, *args, **kwargs):
        ret = super().read(*args, **kwargs)
        if ret:
            self._check()
        return ret

    def read_file(self, *args, **kwargs):
        super().read_file(*args, **kwargs)
        self._check()

    def write(self, fileobject, space_around_delimiters=False):
        return super().write(fileobject, space_around_delimiters=space_around_delimiters)

    def get(self, option, **kwargs):
        return super().get(self.SECTION, option, **kwargs)

    def getboolean(self, *args, **kwargs):
        # FIXME: support for fallback=...
        return self.get(*args, **kwargs) == 'true'

    def getlist(self, *args, **kwargs):
        """ Gets and splits a list value according to the spec """
        escaped = False
        current = ''
        for c in self.get(*args, **kwargs):
            if escaped:
                current += c
                escaped = False
            elif c == ';':
                yield current
                current = ''
            elif c == '\\':
                escaped = True
            else:
                current += c
        if current:
            yield current

    def set(self, option, value):
        return super().set(self.SECTION, option, value)

    def setbool(self, option, value):
        return self.set(option, 'true' if value else 'false')

    def valid_for_desktop(self, desktop=None):
        """ Checks whether the entry should show in the given desktop
        environment.  If `desktop` is None (the default), this function tries
        to detect the current environment. """

        only_show_in = [v.casefold() for v in self.getlist('OnlyShowIn', fallback='')]
        not_show_in = [v.casefold() for v in self.getlist('NotShowIn', fallback='')]
        if desktop is None:
            desktop = os.environ.get('XDG_CURRENT_DESKTOP', '')
        desktop = desktop.casefold()
        return not ((only_show_in and desktop not in only_show_in) or
                    (not_show_in and desktop in not_show_in))

    def is_hidden(self):
        """ Checks whether the entry is hidden (has Hidden=true) """
        return self.get('Hidden', fallback='false') == 'true'

    def should_show(self, desktop=None):
        """ Checks whether this entry should show, that is is not hiden and
        valid for the given desktop """
        return not self.is_hidden() and self.valid_for_desktop(desktop)

    def get_type(self):
        return self.get('Type')

    def get_exec(self):
        # FIXME: syntax is not exactly shlex, but meh
        return shlex.split(self.get('Exec'))

    @staticmethod
    def get_autostart_enabled_key(desktop=None):
        if desktop is None:
            desktop = os.environ.get('XDG_CURRENT_DESKTOP', '')
        if desktop:
            return 'X-%s-Autostart-enabled' % desktop
        return None

    def is_enabled_for_autostart(self, desktop=None):
        if not self.should_show():
            return False
        key = self.get_autostart_enabled_key(desktop=desktop)
        if key:
            return self.get(key, fallback='true') != 'false'
        return True


def list_autostart_files():
    """ list autostart files paths that should be considered.  This does not
    take into account the file desktop file's content. """

    found = {}
    for d in autostart_paths():
        try:
            files = os.listdir(d)
        except FileNotFoundError:
            continue

        for f in files:
            path = os.path.join(d, f)

            if f in found:
                logging.debug('skipping %r because it is overridden by %r',
                              path, found[f])
                continue

            found[f] = path
            yield path


def list_autostart_config(*args, skip_hidden=True, **kwargs):
    """ Lists autostart path and entry that should be considered, taking into
    account Hidden and OnlyShowIn/NotShowIn keys.
    Arguments are forwarded to DesktopEntry.valid_for_desktop() """

    for path in list_autostart_files():
        entry = DesktopEntry()
        try:
            with open(path, 'r') as fp:
                try:
                    entry.read_file(fp)
                except configparser.Error as ex:
                    logging.debug('skipping %r because it is not a valid Desktop Entry file',
                                  path)
                    continue
        except OSError as ex:
            logging.exception(ex)
            continue

        if entry.get_type() != 'Application':
            logging.debug('skipping %r because it is not Type=Application', path)
            continue
        if (skip_hidden and not entry.is_enabled_for_autostart(*args, **kwargs)) or \
           not entry.valid_for_desktop(*args, **kwargs):
            logging.debug('skipping %r because it should not show', path)
            continue

        yield path, entry


def find_exec(match, entries):
    if os.path.sep in match:
        transform = str
    else:
        transform = os.path.basename

    for path, entry in entries:
        try:
            cmd_argv = entry.get_exec()
            if match == transform(cmd_argv[0]):
                logging.debug("Found match for %r in %r: %s",
                              match, path, ' '.join(shlex.quote(arg) for arg in cmd_argv))
                return path, entry
        except configparser.Error as ex:
            logging.exception(ex)
    return None, None


def find_autostart_exec(match, *args, **kwargs):
    return find_exec(match, list_autostart_config(*args, **kwargs))


def action_exec_status(args):
    path, entry = find_autostart_exec(args.exec)
    return path is not None


def data_set_value(data, key, value):
    # We don't use configparser to write the value back, because it strips
    # comments and such
    regex = re.compile(r'^([:blank:]*' + re.escape(key) + '[:blank:]*=[:blank:]*).*$',
                       flags=re.MULTILINE)

    if regex.search(data):
        data = regex.sub(r'\1' + value, data)
    else:
        if not data.endswith('\n'):
            data += '\n'
        data += '%s=%s\n' % (key, value)

    return data


def file_set_values(path, keys_values_dict):
    with open(path, 'r') as fp:
        data = fp.read()

    # We don't use configparser to write the value back, because it strips
    # comments and such
    new_data = data
    for key, value in keys_values_dict.items():
        new_data = data_set_value(new_data, key, value)
    if new_data == data:
        return  # nothing to do, data didn't change

    write_path = os.path.join(get_user_autostart_path(), os.path.basename(path))
    with open(write_path, 'w') as fp:
        fp.write(new_data)


def action_exec_enable(args):
    path, entry = find_autostart_exec(args.exec, skip_hidden=False)
    if entry and entry.is_enabled_for_autostart():
        # Enabled already, nothing to do
        logging.debug('%r is already enabled, nothing to do', path)
        return True

    def list_applications():
        applications_dir = '/usr/share/applications'
        try:
            files = os.listdir(applications_dir)
        except FileNotFoundError:
            return

        for path in (os.path.join(applications_dir, name) for name in files):
            entry = DesktopEntry()
            try:
                with open(path, 'r') as fp:
                    try:
                        entry.read_file(fp)
                    except configparser.Error as ex:
                        logging.debug('skipping %r because it is not a valid Desktop Entry file',
                                      path)
                        continue
            except IsADirectoryError:
                continue
            except OSError as ex:
                logging.exception(ex)
                continue

            if entry.get_type() != 'Application':
                logging.debug('skipping %r because it is not Type=Application', path)
                continue
            if not entry.valid_for_desktop():
                logging.debug('skipping %r because it should not show in the current desktop', path)
                continue

            yield path, entry

    if not path:
        # try and find a matching application
        logging.debug('no autostart file matching %r, looking for applications',
                      args.exec)
        path, entry = find_exec(args.exec, list_applications())

    if path:
        key = entry.get_autostart_enabled_key()
        if key:
            value = 'true'
        else:
            key, value = 'Hidden', 'false'
        file_set_values(path, {key: value})
        return True

    # If all else failed, create a dummy-ish desktop file to run the queried
    # command
    write_path = os.path.join(get_user_autostart_path(),
                              'autogenerated-%s.desktop' % os.path.basename(args.exec))
    with open(write_path, 'w') as fp:
        fp.write('[Desktop Entry]\n'
                 'Type=Application\n'
                 'Name={0}\n'
                 'Exec={0}\n'.format(args.exec))

    return True


def action_exec_disable(args):
    # FIXME: disable *all* matches?
    path, entry = find_autostart_exec(args.exec)
    if not path:
        # Not enabled, nothing to do
        return True

    # We don't use configparser to write the value back, because it strips
    # comments and such
    key = entry.get_autostart_enabled_key()
    if key:
        value = 'false'
    else:
        key, value = 'Hidden', 'true'
    file_set_values(path, {key: value})

    return True


def main():
    import sys
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument('--log-level', default='WARNING',
                        choices=['DEBUG', 'INFO', 'WARNING', 'ERROR', 'CRITICAL'],
                        help='Configure the logging level.')
    parser.set_defaults(handler=lambda args: parser.print_usage())
    subparsers = parser.add_subparsers()

    sub = subparsers.add_parser('exec-status', help='Checks whether an executable is in the autostart list')
    sub.set_defaults(handler=action_exec_status)
    sub.add_argument('exec', help='Executable to check for')

    sub = subparsers.add_parser('exec-enable', help='Enables an executable in the autostart list. '
                                                    'The executable must either be in the list but '
                                                    'disabled, or match a desktop file.  Only the '
                                                    'first desktop file matching the executable will '
                                                    'be enabled.')
    sub.set_defaults(handler=action_exec_enable)
    sub.add_argument('exec', help='Executable to enable')

    sub = subparsers.add_parser('exec-disable', help='Disables an executable in the autostart list. '
                                                     'Only the first desktop file matching the '
                                                     'executable will be disabled.')
    sub.set_defaults(handler=action_exec_disable)
    sub.add_argument('exec', help='Executable to disable')

    args = parser.parse_args()

    logging.basicConfig(level=getattr(logging, args.log_level.upper()))

    sys.exit(0 if args.handler(args) else 1)


if __name__ == '__main__':
    main()

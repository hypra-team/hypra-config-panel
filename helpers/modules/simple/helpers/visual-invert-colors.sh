#!/bin/sh
# enables/disables Compiz neg plugin's toggle_screen_by_default, making sure
# the plugin is actually enabled if the value is true

set -e

case "$1" in
  get)
    compiz-setting.py plugin-enabled neg &&
    compiz-setting.py get neg s0_toggle_screen_by_default;;
  set)
    test "$2" = true && compiz-setting.py enable-plugin neg
    compiz-setting.py set neg s0_toggle_screen_by_default "$2";;
  status)
    compiz-setting.py status core as_active_plugins &&
    compiz-setting.py status neg s0_toggle_screen_by_default;;
  *) exit 1;;
esac

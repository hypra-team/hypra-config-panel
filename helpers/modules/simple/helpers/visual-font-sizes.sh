#!/bin/sh
# adjusts all desktop's font sizes at once

set -e

FONT_APPS_FACTOR=18
FONT_DESKTOP_FACTOR=18
FONT_DOCS_FACTOR=16
FONT_MONO_FACTOR=16
FONT_TITLE_FACTOR=30

FONT_APPS_KEY="org.mate.interface font-name"
FONT_DESKTOP_KEY="org.mate.caja.desktop font"
FONT_DOCS_KEY="org.mate.interface document-font-name"
FONT_MONO_KEY="org.mate.interface monospace-font-name"
FONT_TITLE_KEY="org.mate.Marco.general titlebar-font"
CAJA_ZOOM_KEY="org.mate.caja.icon-view default-zoom-level"

# set_font_size GROUP KEY FACTOR SIZE
set_font_size() {
  font=$(gsettings get "$1" "$2" | sed -r "s/^'(.*) [0-9]+'\$/\1/")
  gsettings set "$1" "$2" "$font $(($3 * $4 / FONT_APPS_FACTOR))"
}

case "$1" in
  get)
    # TODO: improve this if factors are off
    gsettings get $FONT_APPS_KEY | sed -r "s/^.* ([0-9]+)'\$/\1/"
    ;;
  set)
    set_font_size $FONT_APPS_KEY "$FONT_APPS_FACTOR" "$2"
    set_font_size $FONT_DESKTOP_KEY "$FONT_DESKTOP_FACTOR" "$2"
    set_font_size $FONT_DOCS_KEY "$FONT_DOCS_FACTOR" "$2"
    set_font_size $FONT_MONO_KEY "$FONT_MONO_FACTOR" "$2"
    set_font_size $FONT_TITLE_KEY "$FONT_TITLE_FACTOR" "$2"

    # adjust the icon view (desktop in particular) zoom level
    default_zoom=larger
    test "$2" -le 22 && default_zoom=large
    test "$2" -le 16 && default_zoom=standard
    test "$2" -le 12 && default_zoom=small
    test "$2" -le  8 && default_zoom=smaller
    gsettings set $CAJA_ZOOM_KEY $default_zoom
    ;;
  debug-get)
    set -x
    gsettings get $FONT_APPS_KEY
    gsettings get $FONT_DESKTOP_KEY
    gsettings get $FONT_DOCS_KEY
    gsettings get $FONT_MONO_KEY
    gsettings get $FONT_TITLE_KEY
    gsettings get $CAJA_ZOOM_KEY
    ;;
  reset)
    gsettings reset $FONT_APPS_KEY
    gsettings reset $FONT_DESKTOP_KEY
    gsettings reset $FONT_DOCS_KEY
    gsettings reset $FONT_MONO_KEY
    gsettings reset $FONT_TITLE_KEY
    gsettings reset $CAJA_ZOOM_KEY
    ;;
  status)
    test x$(gsettings writable $FONT_APPS_KEY) = xtrue &&
    test x$(gsettings writable $FONT_DESKTOP_KEY) = xtrue &&
    test x$(gsettings writable $FONT_DOCS_KEY) = xtrue &&
    test x$(gsettings writable $FONT_MONO_KEY) = xtrue &&
    test x$(gsettings writable $FONT_TITLE_KEY) = xtrue &&
    test x$(gsettings writable $CAJA_ZOOM_KEY) = xtrue
    ;;
  *) exit 1;;
esac

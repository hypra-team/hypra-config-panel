#!/bin/sh

set -e

case "$1" in
  get)
    pgrep -u "$USER" -x orca >/dev/null && echo true || echo false
    # we don't test the autostart status, because we can't represent "half-yes",
    # so we just use the current status, and will set the autostart upon "set"
    #autostart.py exec-status orca && echo true || echo false
    ;;
  set)
    if test "${HCP_VALUE}" = true; then
      orca-setting.py set enableSpeech true
      orca --replace &
      autostart=enable
    else
      pkill -u "$USER" -x orca || test "$?" = 1
      autostart=disable
    fi
    # Set autostart behavior, but don't fail if that doesn't work
    autostart.py "exec-$autostart" orca || :
    ;;
  status)
    type orca >/dev/null
    orca-setting.py status enableSpeech
    ;;
  *)
    echo "Unknown action \"$1\"" >&2
    exit 1;;
esac

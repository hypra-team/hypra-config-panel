#!/usr/bin/env python3
# -*- coding: utf-8; -*-
#
#  Helper script to set the MATE theme.
#
#  Copyright (C) 2021  Colomban Wendling <cwendling@hypra.fr>
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.

import sys
import os
import configparser
import logging
import gettext
import struct

from gi.repository import Gio

from hcp.config import GETTEXT_PACKAGE, LOCALEDIR


# setup i18n
gettext.install(GETTEXT_PACKAGE, LOCALEDIR)


class MateThemeConfigParser(configparser.ConfigParser):
    def __init__(self, *args, **kwargs):
        real_kwargs = {'interpolation': None, 'strict': False}
        real_kwargs.update(kwargs)
        super().__init__(*args, **real_kwargs)


def configparser_get_colorscheme(*args, **kwargs):
    value = configparser.ConfigParser.get(*args, **kwargs)
    if value:
        return value.replace(',', '\n')
    return value


class MateTheme:
    settings_map = {
        'GtkTheme': ['org.mate.interface', 'gtk-theme', str],
        'GtkColorScheme': ['org.mate.interface', 'gtk-color-scheme', 'colorscheme'],
        'MetacityTheme': ['org.mate.Marco.general', 'theme', str],
        'IconTheme': ['org.mate.interface', 'icon-theme', str],
        'CursorTheme': ['org.mate.peripherals-mouse', 'cursor-theme', str],
        'CursorSize': ['org.mate.peripherals-mouse', 'cursor-size', int],
        'NotificationTheme': ['org.mate.NotificationDaemon', 'theme', str],
        'ApplicationFont': ['org.mate.interface', 'font-name', str],
        'DocumentsFont': ['org.mate.interface', 'document-font-name', str],
        'DesktopFont': ['org.mate.caja.desktop', 'font', str],
        'WindowTitleFont': ['org.mate.Marco.general', 'titlebar-font', str],
        'MonospaceFont': ['org.mate.interface', 'monospace-font-name', str],
        'BackgroundImage': ['org.mate.background', 'picture-filename', str],
    }
    gsettings_map = {
        str: [Gio.Settings.get_string, Gio.Settings.set_string],
        'colorscheme': [Gio.Settings.get_string, Gio.Settings.set_string],
        int: [Gio.Settings.get_int, Gio.Settings.set_int],
    }
    configparser_map = {
        str: [configparser.ConfigParser.get, None],
        'colorscheme': [configparser_get_colorscheme, None],
        int: [configparser.ConfigParser.getint, None],
    }

    @classmethod
    def _gsetting_get(schema, setting):
        pass

    @staticmethod
    def _theme_paths():
        """ returns locations that contain themes, in priority order """
        return [os.path.expanduser('~/.themes'), '/usr/share/themes']

    @classmethod
    def _get_conf_for_theme(cls, theme_name):
        """ gets a ConfigParser object for the given theme """
        theme_conf = MateThemeConfigParser()
        for path in cls._theme_paths():
            theme_path = os.path.join(path, theme_name, 'index.theme')
            if theme_conf.read(theme_path):
                return theme_conf
        return None

    @classmethod
    def set_theme(cls, theme_name):
        """ sets the settings following the theme """
        theme_conf = cls._get_conf_for_theme(theme_name)
        if not theme_conf:
            return False

        for conf_key, desc in cls.settings_map.items():
            value = cls.configparser_map[desc[2]][0](theme_conf,
                                                     'X-GNOME-Metatheme',
                                                     conf_key, fallback=None)
            if value is not None:
                logging.debug('SET %s %s=%s', desc[0], desc[1], value)
                settings = Gio.Settings(schema_id=desc[0])
                cls.gsettings_map[desc[2]][1](settings, desc[1], value)

        Gio.Settings.sync()

        return True

    @classmethod
    def get_current_theme(cls):
        """ tries to infer the current theme name from current settings """
        for path in cls._theme_paths():
            for theme in os.listdir(path):
                theme_path = os.path.join(path, theme, 'index.theme')
                logging.debug('Looking at %s...', theme_path)
                theme_conf = MateThemeConfigParser()
                if not theme_conf.read(theme_path):
                    continue

                for conf_key, desc in cls.settings_map.items():
                    if conf_key.startswith('Cursor'):
                        # we ignore cursor options in matching because we allow
                        # separately tuning them
                        continue

                    conf_value = cls.configparser_map[desc[2]][0](theme_conf, 'X-GNOME-Metatheme', conf_key, fallback=None)
                    if conf_value is not None:
                        settings = Gio.Settings(schema_id=desc[0])
                        settings_value = cls.gsettings_map[desc[2]][0](settings, desc[1])
                        logging.debug('CHK %s vs %s', conf_value, settings_value)
                        if conf_value != settings_value:
                            break
                else:
                    logging.debug('FOUND: %s (%s)', theme, theme_path)
                    return theme
        return None

    @classmethod
    def list_themes(cls):
        for path in cls._theme_paths():
            for theme in os.listdir(path):
                theme_path = os.path.join(path, theme, 'index.theme')
                logging.debug('Looking at %s...', theme_path)
                theme_conf = MateThemeConfigParser()
                if theme_conf.read(theme_path):
                    # We probably should read "Desktop Entry"/"Name" (or
                    # "X-GNOME-Metatheme"/"Name" for some buggy themes),
                    # but it seems fairly unreliable because themes are meh.
                    yield theme

        return True

    @classmethod
    def writable(cls, theme_name=None):
        """ checks if theme settings can be applied """
        conf = cls._get_conf_for_theme(theme_name) if theme_name else None
        for conf_key, desc in cls.settings_map.items():
            if conf and not conf.has_option('X-GNOME-Metatheme', conf_key):
                continue
            settings = Gio.Settings(schema_id=desc[0])
            if not settings.is_writable(desc[1]):
                return False
        return True


def get_theme():
    theme = MateTheme.get_current_theme()
    if theme is None:
        theme = "(Custom)"
    print(theme)
    return True


def set_theme(theme):
    return MateTheme.set_theme(theme)


def list_themes():
    for theme in sorted(MateTheme.list_themes()):
        print(theme)
    return True


def list_filtered():
    maps = {
        "ContrastHigh": _("Black on white"),
        "HighContrastInverse": _("White on black"),
        "Hypra": _("Senior"),
    }
    for theme in sorted(MateTheme.list_themes()):
        if theme in maps:
            print("%s:%s" % (theme, maps[theme]))

    return True


def xcursor_get_sizes(xcursor_path):
    """ Reads the image sizes available in an XCursor cursor file.
    See XCursor manpages for details on the format, e.g. xcursor(3) (e.g.
    https://linux.die.net/man/3/xcursor).
    Note on endianess: although the documentation is not clear on that, the
    libxcursor code manually reads little-endian data. """
    with open(xcursor_path, 'rb') as xcursor_data:
        if xcursor_data.read(4) != b'Xcur':
            raise ValueError('Invalid XCursor file')

        sizes = []

        hsize, version, ntoc = struct.unpack('<LLL', xcursor_data.read(4 * 3))
        skip = hsize - 0x10
        if skip < 0:
            raise ValueError('Invalid XCursor header')
        elif skip > 0:
            xcursor_data.seek(skip, 1)
        logging.debug('XCursor file version %x with %lu entries', version, ntoc)

        if version != 0x10000:
            logging.warning('Unsupported XCursor file version %x, results may '
                            'be erroneous', version)

        # parse the TOC, which contains all the info we need
        for i in range(ntoc):
            etype, subtype, pos = struct.unpack('<LLL', xcursor_data.read(4 * 3))
            logging.debug('- Entry %d: Type is %x, subtype %lu, position %x',
                          i + 1, etype, subtype, pos)
            if etype == 0xfffd0002:  # Image
                sizes.append(subtype)

        return sorted(sizes)


def list_xcursor_themes():
    for path in [os.path.expanduser('~/.icons'), '/usr/share/icons']:
        for theme in os.listdir(path):
            left_ptr_path = os.path.join(path, theme, 'cursors/left_ptr')
            logging.debug('Looking at %s...', left_ptr_path)
            try:
                xcursor_get_sizes(left_ptr_path)
            except (ValueError, NotADirectoryError, FileNotFoundError):
                continue

            # We probably should check for the name in the index.theme, but
            # it seems fairly unreliable because themes are meh.
            yield theme


def list_cursors_themes():
    for theme in sorted(list_xcursor_themes()):
        print(theme)
    return True


def list_cursors_filtered():
    maps = {
        "ComixCursors-Opaque-Black": _("Black"),
        "ComixCursors-Opaque-Blue": _("Blue"),
        "ComixCursors-Opaque-Green": _("Green"),
        "ComixCursors-Opaque-Orange": _("Orange"),
        "ComixCursors-Opaque-Red": _("Red"),
        "ComixCursors-Opaque-White": _("White"),
    }
    all_themes = sorted(list_xcursor_themes())
    filtered_themes = [t for t in all_themes if t in maps] or all_themes
    for theme in filtered_themes:
        if theme in maps:
            name = maps[theme]
        else:
            name = theme.replace('-', ' ')
        print('%s:%s' % (theme, name))
    return True


def get_current_cursor_sizes():
    settings = Gio.Settings(schema_id='org.mate.peripherals-mouse')
    theme = settings.get_string('cursor-theme')

    for path in [os.path.expanduser('~/.icons'), '/usr/share/icons']:
        left_ptr_path = os.path.join(path, theme, 'cursors/left_ptr')
        logging.debug('Looking at %s...', left_ptr_path)
        try:
            return xcursor_get_sizes(left_ptr_path)
        except (ValueError, NotADirectoryError, FileNotFoundError):
            continue

    return None


def get_current_cursor_size_range():
    sizes = get_current_cursor_sizes()

    if sizes is not None:
        print('0:%u' % max(0, len(sizes) - 1))
        return True

    return False


def set_cursor_size(size_id):
    size_id = int(size_id)

    sizes = get_current_cursor_sizes()
    if not sizes or size_id not in range(len(sizes)):
        return False

    settings = Gio.Settings(schema_id='org.mate.peripherals-mouse')
    settings.set_int('cursor-size', sizes[size_id])

    return True


def get_cursor_size():
    sizes = get_current_cursor_sizes()

    settings = Gio.Settings(schema_id='org.mate.peripherals-mouse')
    size = settings.get_int('cursor-size')

    try:
        index = sizes.index(size)
    except ValueError:
        return False

    print(index)
    return True


def is_theme_writable(theme=None):
    return MateTheme.writable(theme)


def usage(exit_code=1, full=False):
    print("USAGE: %s help|get|set|status" % sys.argv[0],
          file=sys.stdout if exit_code == 0 else sys.stderr)
    if full:
        print("""
Applies a MATE theme.

COMMANDS:

get             Gets the current theme name
get-current-cursor-size-range
                Gets the number of sizes the current cursor theme provides
get-cursor-size Gets the index of the current cursor theme size in its sizes
                list
list            List available themes
list-filtered   List available themes (filtered)
list-cursors    List available cursor themes
list-cursors-filtered
                List available cursor themes (filtered)
set THEME       Sets the current theme to THEME.
set-cursor-size INDEX
                Sets the current cursor theme size from its index in the
                cursor size list.
status [THEME]  Checks whether THEME can be selected
help            Show this help.""")
    sys.exit(exit_code)


def main():
    if len(sys.argv) < 2:
        usage()
    if sys.argv[1] == 'get':
        sys.exit(0 if get_theme() else 1)
    elif sys.argv[1] == 'list':
        sys.exit(0 if list_themes() else 1)
    elif sys.argv[1] == 'list-filtered':
        sys.exit(0 if list_filtered() else 1)
    elif sys.argv[1] == 'list-cursors':
        sys.exit(0 if list_cursors_themes() else 1)
    elif sys.argv[1] == 'list-cursors-filtered':
        sys.exit(0 if list_cursors_filtered() else 1)
    elif sys.argv[1] == 'get-current-cursor-size-range':
        sys.exit(0 if get_current_cursor_size_range() else 1)
    elif sys.argv[1] == 'get-cursor-size':
        sys.exit(0 if get_cursor_size() else 1)
    elif sys.argv[1] == 'set-cursor-size' and len(sys.argv) == 3:
        sys.exit(0 if set_cursor_size(sys.argv[2]) else 1)
    elif sys.argv[1] == 'set' and len(sys.argv) == 3:
        sys.exit(0 if set_theme(sys.argv[2]) else 1)
    elif sys.argv[1] == 'status':
        sys.exit(0 if is_theme_writable(sys.argv[2] if len(sys.argv) > 2 else None) else 1)
    elif sys.argv[1] == 'help':
        usage(0, True)
    else:
        usage()


if __name__ == '__main__':
    # setup logging
    try:
        debug_domains = os.environ['G_MESSAGES_DEBUG']
    except KeyError:
        pass
    else:
        if debug_domains == 'all' or 'mate-theme' in debug_domains.split(' '):
            logging.basicConfig(level=logging.DEBUG)

    main()

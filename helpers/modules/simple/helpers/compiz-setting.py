#!/usr/bin/env python3
# coding: utf-8
#
# Copyright 2019-2021 Hypra Team <bugs@hypra.fr>
#
# This file is part of the Hypra project.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, see <http://www.gnu.org/licenses/>.

import os
import xml.etree.ElementTree as ET
import configparser
import logging
try:
    from functools import cache
except ImportError:
    from functools import lru_cache
    cache = lru_cache(maxsize=None)


COMPIZ_RELOADED_BASEPATH = '/usr/share/compiz'


@cache
def find_xml_for_reloaded_plugin(plugin_name):
    filename = '%s.xml' % plugin_name
    for root, dirs, files in os.walk(COMPIZ_RELOADED_BASEPATH):
        for f in files:
            if f == filename:
                return os.path.join(root, f)

    return None


@cache
def load_xml_for_reloaded_plugin(plugin_name):
    xml = find_xml_for_reloaded_plugin(plugin_name)
    if not xml:
        return None
    logging.debug('loading reloaded XML from "%s"', xml)
    return ET.parse(xml)


def xml_read_default_value(node):
    default_node = node.find('./default')
    if default_node is None:
        # Compiz 0.9 has a fake <default_plugins/> tag in its template for the
        # default plugins.  If we use the template, translate it to the actual
        # default value.
        if node.attrib['type'] == 'list' and \
           node.find('./default_plugins') is not None:
            return ['core', ]
        elif node.attrib['type'] in ('key', 'button'):
            return 'Disabled'
        elif node.attrib['type'] == 'edge':
            return 'None'
        return None

    if node.attrib['type'] == 'list':
        l = [''.join(v.itertext()) for v in default_node.findall('./value')]
        if default_node.find('./default_plugins') is not None:
            l += ['core', ]
        return l
    elif node.attrib['type'] == 'color':
        def get_component(n):
            cn = default_node.find('./%s' % n)
            return '%x' % (int(cn.text) // 0x100) if cn else 'ff'

        return '#' + ''.join([get_component('red'),
                              get_component('green'),
                              get_component('blue'),
                              get_component('alpha')])
    elif node.attrib['type'] == 'edge':
        return '|'.join(e.attrib['name'] for e in default_node.findall('./edge')
                        if e.attrib['name'] != 'None') or 'None'
    else:
        data = ''.join(default_node.itertext())
        if node.attrib['type'] == 'bool':
            return data.lower() == 'true'
        elif node.attrib['type'] == 'int':
            return int(data)
        elif node.attrib['type'] == 'float':
            return float(data)
        elif node.attrib['type'] in ('key', 'button'):
            return data.replace('<Control>', '<Primary>') or 'Disabled'
        else:
            return data


def xml_node_for_option(plugin_name, setting):
    root = load_xml_for_reloaded_plugin(plugin_name)
    if root is None:
        return None

    prefix, name = setting.split('_', 1)
    section = 'display' if prefix == 'as' else 'screen'
    return root.find('.//%s//option[@name="%s"]' % (section, name))


def get_reloaded_default_value(plugin_name, setting):
    node = xml_node_for_option(plugin_name, setting)
    return xml_read_default_value(node) if node is not None else None


def get_reloaded_value_type(plugin_name, setting):
    node = xml_node_for_option(plugin_name, setting)
    return node.attrib['type'] if node is not None else None


COMPIZ_VALUE_PARSERS = {
    'int': int,
    'float': float,
    'key': lambda v: str(v).replace('<Control>', '<Primary>') if v else 'Disabled',
    'button': lambda v: str(v).replace('<Control>', '<Primary>') if v else 'Disabled',
    'match': str,
    'string': str,
    'list': lambda s: s.rstrip(';').split(';'),
    'bool': lambda s: s.lower() == 'true',
    'edge': str,
    'color': str,
}
COMPIZ_VALUE_FORMATTERS = {
    'int': str,
    'float': str,
    'key': lambda v: str(v).replace('<Control>', '<Primary>') if v else 'Disabled',
    'button': lambda v: str(v).replace('<Control>', '<Primary>') if v else 'Disabled',
    'match': str,
    'string': str,
    'list': lambda v: (';'.join(str(i) for i in v) + ';') if v else '',
    'bool': lambda v: ['false', 'true'][bool(v)],
    'edge': lambda v: str(v) or 'None',
    'color': str,
}


def format_compiz_value(val, compiz_type):
    return COMPIZ_VALUE_FORMATTERS[compiz_type](val)


def parse_compiz_value(val, compiz_type):
    return COMPIZ_VALUE_PARSERS[compiz_type](val)


class CompizReloadedConfigParser(configparser.ConfigParser):
    def __init__(self, **kwargs):
        super().__init__(strict=False, interpolation=None,
                         converters=COMPIZ_VALUE_PARSERS, **kwargs)

    def get_typed(self, option_type, plugin, option):
        raw = super().get(plugin, option)
        return COMPIZ_VALUE_PARSERS[option_type](raw)

    def set_typed(self, option_type, plugin, option, value):
        raw_value = COMPIZ_VALUE_FORMATTERS[option_type](value)
        return super().set(plugin, option, raw_value)


def get_reloaded_option(args, plugin_name, option):
    option_type = get_reloaded_value_type(plugin_name, option)
    if not option_type:
        return None, None

    config = CompizReloadedConfigParser()
    config.read([args.presets_path, args.user_path])

    if config.has_option(plugin_name, option):
        value = config.get_typed(option_type, plugin_name, option)
    else:
        value = get_reloaded_default_value(plugin_name, option)

    return value, option_type


def setting_get(args):
    value, option_type = get_reloaded_option(args, args.plugin, args.setting)
    if not option_type:
        logging.warning('Unknown setting "%s" for plugin "%s"',
                        args.setting, args.plugin)
        return False

    # print the value using Compiz format, and the round-trip normalizes it as well
    print(format_compiz_value(value, option_type))
    return True


def set_reloaded_option(args, plugin_name, option, option_type, value):
    """ Writes an option.
    args: Main program arguments
    plugin_name: name of the plugin
    option: name of the option (with the as_ or s0_ prefix)
    option_type: Compiz option type
    value: Python value """
    config = CompizReloadedConfigParser()
    config.read(args.user_path)

    if not config.has_section(plugin_name):
        config.add_section(plugin_name)
    config.set_typed(option_type, plugin_name, option, value)

    with open(args.user_path, 'w') as fp:
        config.write(fp)
    return True


def setting_set(args):
    option_type = get_reloaded_value_type(args.plugin, args.setting)
    if not option_type:
        logging.warning('Unknown setting "%s" for plugin "%s"',
                        args.setting, args.plugin)
        return False

    # read the user value as a Compiz value -- which validates the format as well
    # TODO: when setting core/as_active_plugins, validate the plugin order and
    # dependencies not to risk a broken Compiz.  See Compiz settings migration
    # code in the upgrader.
    value = parse_compiz_value(args.value, option_type)

    return set_reloaded_option(args, args.plugin, args.setting, option_type, value)


def setting_reset(args):
    config = CompizReloadedConfigParser()
    config.read(args.user_path)

    if config.has_option(args.plugin, args.setting):
        config.remove_option(args.plugin, args.setting)
        if not config.options(args.plugin):
            config.remove_section(args.plugin)

        with open(args.user_path, 'w') as fp:
            config.write(fp)

    return True


def setting_status(args):
    # check if Compiz seems to be properly installed and found
    if not find_xml_for_reloaded_plugin('core'):
        return False

    path = args.user_path
    while path != '/' and not os.path.exists(path):
        path = os.path.dirname(path)
    return os.access(path, os.W_OK)


def setting_plugin_enabled(args):
    value, option_type = get_reloaded_option(args, 'core', 'as_active_plugins')
    return args.plugin in value if value else False


def setting_enable_plugin(args):
    class BeforeCache:
        """ An indexable object holding the list of plugins that should be
            loaded after the key, e.g. BeforeCache["core"] lists everything
            that should appear *after* "core" """
        def __init__(self):
            self._cache = {}
            self.insert('core', 'ccp')

        def __getitem__(self, key):
            if not key in self._cache:
                self._cache[key] = (False, [])
            return self._cache[key][1]

        def validate(self, key):
            self[key][0] = True

        def insert(self, plugin, dep):
            if plugin == dep:
                logging.warning('Plugin %s depends on itself!?', plugin)
            else:
                deps = self[plugin]
                if dep not in deps:
                    deps.append(dep)

        def get_deps(self, plugin):
            if plugin in self._cache and self._cache[plugin][0]:
                return self._cache[plugin][1]

            self.validate(plugin)

            root = load_xml_for_reloaded_plugin(plugin)
            if not xml:
                logging.warning('No such plugin "%s"', plugin)
            else:
                for node in root.findall('.//plugin/deps/requirement/plugin'):
                    self.insert(node.text, plugin)
                for node in root.findall('.//plugin/deps/relation[@type="before"]/plugin'):
                    logging.debug('Plugin %s should be loaded before %s', plugin, node.text)
                    self.insert(plugin, node.text)
                for node in root.findall('.//plugin/deps/relation[@type="after"]/plugin'):
                    logging.debug('Plugin %s should be loaded after %s', plugin, node.text)
                    self.insert(node.text, plugin)

            return self[plugin]

    available_plugins = []
    before_cache = BeforeCache()

    def add_reloaded_plugin(name):
        nonlocal available_plugins, before_cache
        if name not in available_plugins:
            root = load_xml_for_reloaded_plugin(name)
            if root is None:
                logging.warning('Plugin "%s" not found', name)
                return False
            else:
                logging.info('resolving dependencies for plugin %s', name)
                for node in root.findall('.//plugin/deps/requirement/plugin'):
                    dep = node.text
                    if dep == name:
                        logging.warning('Plugin %s depends on itself!?', name)
                    elif dep not in available_plugins:
                        logging.info('Adding plugin %s as a dependency for %s', dep, name)
                        add_reloaded_plugin(dep)

                if name not in available_plugins:
                    # find where to insert the plugin. For that, find the
                    # earliest plugin that depends on us and go there.
                    pos = len(available_plugins)
                    for plugin in reversed(available_plugins):
                        if plugin in before_cache[name]:
                            pos = available_plugins.index(plugin)

                    available_plugins.insert(pos, name)

        return True

    value, option_type = get_reloaded_option(args, 'core', 'as_active_plugins')
    if option_type != 'list' or not isinstance(value, list):
        logging.error('Cannot find the list of active plugins')
        return False

    available_plugins = list(value)
    if not args.no_dependencies:
        if not add_reloaded_plugin(args.plugin):
            return False
    elif args.plugin not in available_plugins:
        if not find_xml_for_reloaded_plugin(args.plugin):
            logging.warning('Plugin "%s" not found', args.plugin)
            return False
        available_plugins.append(args.plugin)

    if available_plugins == value:
        logging.debug('No changes')
        return True

    logging.debug('setting active plugins to: %r', available_plugins)
    return set_reloaded_option(args, 'core', 'as_active_plugins', 'list',
                               available_plugins)


def setting_list(args):
    def list_options(xml, plugin_name, group, opt_prefix):
        options = [node for node in xml.findall('.//%s//option[@name]' % group)
                   if node.attrib['type'] != 'action']
        if not options:
            return
        print('\t%s options:' % group)
        for opt in options:
            print('\t\t%s' % opt.attrib['name'], end='')
            if args.type:
                print(' (%s)' % opt.attrib['type'], end='')
            if args.value:
                value, value_type = get_reloaded_option(args, plugin_name,
                                                        opt_prefix + opt.attrib['name'])
                print(' = %s' % format_compiz_value(value, value_type), end='')
            print()

    def list_plugin_options(plugin_name):
        xml = load_xml_for_reloaded_plugin(plugin_name)
        if xml is None:
            return False

        print('%s:' % plugin_name)
        list_options(xml, plugin_name, 'display', 'as_')
        list_options(xml, plugin_name, 'screen', 's0_')
        return True

    if args.plugin is not None:
        return list_plugin_options(args.plugin)
    else:
        for root, dirs, files in os.walk(COMPIZ_RELOADED_BASEPATH):
            for f in files:
                name, ext = os.path.splitext(f)
                if ext != '.xml':
                    continue
                list_plugin_options(name)

    return True


if __name__ == '__main__':
    import sys
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument('--user-path', metavar='PATH', help='User configuration file [default: %(default)s]',
                        default=os.path.expanduser('~/.config/compiz/compizconfig/Default.ini'))
    parser.add_argument('--log-level', default='WARNING',
                        choices=['DEBUG', 'INFO', 'WARNING', 'ERROR', 'CRITICAL'],
                        help='Configure the logging level.')
    parser.add_argument('--system-path', help='path under which find Compiz-Reloaded XML files [default: %(default)s]',
                        default=COMPIZ_RELOADED_BASEPATH)
    parser.add_argument('--presets-path', help='path to Compiz-Reloaded presets file [default: %(default)s]',
                        default='/etc/compizconfig/presets.ini')
    subparsers = parser.add_subparsers()

    parser_get = subparsers.add_parser('get', help='Get a value')
    parser_get.set_defaults(handler=setting_get)
    parser_get.add_argument('plugin', help='Compiz plugin to which the setting belongs')
    parser_get.add_argument('setting', help='Compiz plugin\'s setting')

    parser_set = subparsers.add_parser('set', help='Set a value')
    parser_set.set_defaults(handler=setting_set)
    parser_set.add_argument('plugin', help='Compiz plugin to which the setting belongs')
    parser_set.add_argument('setting', help='Compiz plugin\'s setting')
    parser_set.add_argument('value', help='Setting value')

    parser_reset = subparsers.add_parser('reset', help='Reset a value to its default')
    parser_reset.set_defaults(handler=setting_reset)
    parser_reset.add_argument('plugin', help='Compiz plugin to which the setting belongs')
    parser_reset.add_argument('setting', help='Compiz plugin\'s setting')

    parser_status = subparsers.add_parser('status', help='Check whether a setting can be written')
    parser_status.set_defaults(handler=setting_status)
    parser_status.add_argument('plugin', help='Compiz plugin to which the setting belongs', nargs='?')
    parser_status.add_argument('setting', help='Compiz plugin\'s setting', nargs='?')

    parser_status = subparsers.add_parser('list', help='List plugins and settings')
    parser_status.set_defaults(handler=setting_list)
    parser_status.add_argument('plugin', help='Compiz plugin to which the setting belongs', nargs='?')
    parser_status.add_argument('--value', help='Show the value next to the setting', action='store_true')
    parser_status.add_argument('--type', help='Show the type next to the setting', action='store_true')

    parser_status = subparsers.add_parser('plugin-enabled', help='Check if a plugin is enabled')
    parser_status.set_defaults(handler=setting_plugin_enabled)
    parser_status.add_argument('plugin', help='Compiz plugin to check for')

    parser_status = subparsers.add_parser('enable-plugin', help='Enable a plugin')
    parser_status.set_defaults(handler=setting_enable_plugin)
    parser_status.add_argument('plugin', help='Compiz plugin to enable')
    # make it hard to enable --no-dependencies by mistake, by requiring an
    # explicit yes/no argument, avoiding the pitfalls of abbreviated options
    def bool_type(s):
        if s.lower() not in ['yes', 'no']:
            raise TypeError()
        return s.lower() == 'yes'
    parser_status.add_argument('--no-dependencies',
                               help=('Do not resolve dependencies.  '
                                     'This can be dangerous as missing '
                                     'dependencies can prevent Compiz from '
                                     'starting.'),
                               default='no', metavar='{yes,no}', type=bool_type)

    args = parser.parse_args()

    logging.basicConfig(level=getattr(logging, args.log_level.upper()))

    COMPIZ_RELOADED_BASEPATH = args.system_path

    sys.exit(0 if args.handler(args) else 1)

#!/usr/bin/env python3
# coding: utf-8
#
# Copyright 2021 Colomban Wendling <cwendling@hypra.fr>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, see <http://www.gnu.org/licenses/>.

import os
import enum
import logging

import gi
gi.require_version('Pango', '1.0')
gi.require_version('PangoCairo', '1.0')
from gi.repository import Gio
from gi.repository import Pango
from gi.repository import PangoCairo


class FontKey(enum.Enum):
    APPLICATIONS = enum.auto()
    DESKTOP = enum.auto()
    DOCUMENTS = enum.auto()
    WINDOWS = enum.auto()


FONT_KEYS = {
    FontKey.APPLICATIONS: ("org.mate.interface", "font-name"),
    FontKey.DESKTOP: ("org.mate.caja.desktop", "font"),
    FontKey.DOCUMENTS: ("org.mate.interface", "document-font-name"),
    FontKey.WINDOWS: ("org.mate.Marco.general", "titlebar-font"),
}


FAMILIES = [
    "Arial",
    "Cambria",
    "Cantarell",
    "Luciole",
    "OpenDyslexic",
    "DejaVu Sans",
    "Georgia",
]


def do_get(args):
    schema, key = FONT_KEYS[FontKey.APPLICATIONS]
    settings = Gio.Settings(schema=schema)
    font = settings.get_string(key)

    pfd = Pango.FontDescription(font)
    print(pfd.get_family())
    return True


def do_set(args):
    for schema, key in FONT_KEYS.values():
        settings = Gio.Settings(schema=schema)
        value = settings.get_string(key)
        pfd = Pango.FontDescription(value)
        pfd.set_family(args.font)
        settings.set_string(key, pfd.to_string())
        settings.sync()
    return True


def do_reset(args):
    for schema, key in FONT_KEYS.values():
        settings = Gio.Settings(schema=schema)
        settings.reset(key)
        settings.sync()
    return True


def do_status(args):
    for schema, key in FONT_KEYS.values():
        settings = Gio.Settings(schema=schema)
        if not settings.is_writable(key):
            return False
    return True


def do_list(args):
    font_map = PangoCairo.font_map_get_default()
    for family in sorted(font_map.list_families(), key=lambda v: v.get_name()):
        if family.get_name() not in FAMILIES:
            continue
        print('{0}:{0}'.format(family.get_name()))
    return True


if __name__ == '__main__':
    import sys
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument('--log-level', default='WARNING',
                        choices=['DEBUG', 'INFO', 'WARNING', 'ERROR', 'CRITICAL'],
                        help='Configure the logging level.')
    subparsers = parser.add_subparsers()

    parser_get = subparsers.add_parser('get', help='Get the current font family')
    parser_get.set_defaults(handler=do_get)

    parser_set = subparsers.add_parser('set', help='Set the font family')
    parser_set.set_defaults(handler=do_set)
    parser_set.add_argument('font', help='Font family to use')

    parser_reset = subparsers.add_parser('reset', help='Reset fonts to the default')
    parser_reset.set_defaults(handler=do_reset)

    parser_status = subparsers.add_parser('status', help='Check whether changing fonts is supported')
    parser_status.set_defaults(handler=do_status)

    parser_status = subparsers.add_parser('list', help='List supported fonts')
    parser_status.set_defaults(handler=do_list)

    args = parser.parse_args()

    logging.basicConfig(level=getattr(logging, args.log_level.upper()))

    sys.exit(0 if args.handler(args) else 1)

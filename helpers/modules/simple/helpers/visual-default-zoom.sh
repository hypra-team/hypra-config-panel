#!/bin/sh
# enables/disables Compiz ezoom plugin's s0_zoom_spec_startup option, making
# sure the plugin is actually enabled if the value is true

set -e

case "$1" in
  get)
    compiz-setting.py plugin-enabled ezoom &&
    compiz-setting.py get ezoom s0_zoom_spec_startup;;
  set)
    test "$2" -gt 1 && compiz-setting.py enable-plugin ezoom
    compiz-setting.py set ezoom s0_zoom_spec_startup "$2";;
  status)
    (compiz-setting.py plugin-enabled ezoom ||
     compiz-setting.py status core as_active_plugins) &&
    compiz-setting.py status ezoom s0_zoom_spec_startup;;
  *) exit 1;;
esac

#!/bin/sh
# enables/disables Compiz showmouse plugin's as_activate_at_startup option, making
# sure the plugin is actually enabled if the value is true

set -e

case "$1" in
  get)
    compiz-setting.py plugin-enabled showmouse &&
    compiz-setting.py get showmouse as_activate_at_startup;;
  set)
    test "$2" = true && compiz-setting.py enable-plugin showmouse
    compiz-setting.py set showmouse as_activate_at_startup "$2";;
  status)
    (compiz-setting.py plugin-enabled showmouse ||
     compiz-setting.py status core as_active_plugins) &&
    compiz-setting.py status showmouse as_activate_at_startup;;
  *) exit 1;;
esac

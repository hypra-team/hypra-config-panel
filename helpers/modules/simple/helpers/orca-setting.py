#!/usr/bin/env python3
# -*- coding: utf-8; -*-
#
#  Helper script to read and write Orca settings.
#
#  Copyright (C) 2021  Colomban Wendling <cwendling@hypra.fr>
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os
import sys
import json
import locale
import subprocess
import logging

import speechd

try:
    # This is just to silence importing Orca
    import gi
    gi.require_version('Gdk', '3.0')
    gi.require_version('Gtk', '3.0')

    from orca import settings_manager
    from orca import script_manager
    HAVE_ORCA = True
except ImportError:
    HAVE_ORCA = False


ALL_VOICE_TYPES = ['default', 'hyperlink', 'system', 'uppercase' ]


def get_starting_profile(setting, manager, default='default'):
    print(manager.getSetting(setting)[1])
    return True


def set_starting_profile(setting, raw_value, manager):
    for profile in manager.availableProfiles():
        if profile[1] == raw_value:
            break
    else:
        return False, False, None, None, None

    manager.setStartingProfile(profile)
    return True, False, None, None, None


def get_generic_setting(setting, manager, default=None):
    scopes = setting.split('.')

    node = manager.getSetting(scopes.pop(0))
    if node and scopes:
        leaf = scopes.pop()
        for scope in scopes:
            node = node.get(scope, {})
        node = node.get(leaf, None)

    if node is not None:
        print(json.dumps(node))
    elif default is not None:
        print(default)
    return True


def _dict_set_scoped(dct, scope, value):
    scopes = scope.split('.')
    leaf = scopes.pop()
    for scope in scopes:
        if scope not in dct:
            dct[scope] = {}
        dct = dct[scope]
    dct[leaf] = value


def get_voice(setting, manager, default=None):
    voice = setting.split('.', 2)[1]
    if voice == 'all':
        voice = 'default'

    setting_module = (manager.getSetting('speechServerInfo') or [None, None])[1]
    setting_name = (manager.getSetting('voices') or {}).get(voice, {}).get('family', {}).get('name', None)
    setting_language = (manager.getSetting('voices') or {}).get(voice, {}).get('family', {}).get('lang', None)
    setting_dialect = (manager.getSetting('voices') or {}).get(voice, {}).get('family', {}).get('dialect', None)

    if not setting_language:
        family_locale, encoding = locale.getdefaultlocale()

        setting_language, setting_dialect = '', ''
        if family_locale:
            language_values = family_locale.split('_', 2)
            setting_language = language_values.pop(0)
            if language_values:
                setting_dialect = language_values.pop(0)

    client = speechd.SSIPClient(sys.argv[0])
    if setting_module is not None:
        for module in client.list_output_modules():
            if module == setting_module:
                client.set_output_module(module)
                break
    module = client.get_output_module()
    voice = None
    for name, lang, variant in client.list_synthesis_voices():
        if name == setting_name:
            voice = name
            break
        if (voice is None and (not setting_language or lang.casefold().startswith(setting_language.casefold()))) or \
            (lang.casefold() == setting_language.casefold() and variant.casefold() == setting_dialect.casefold()):
            voice = name
    if not voice:
        voice = '%s default voice' % module

    if module and voice:
        print('%s@%s' % (voice, module))
    elif default is not None:
        print(default)
    else:
        return False
    return True


def set_voice(setting, raw_value, manager):
    """ Sets the voice based on a "voice@module" argument, trying to match
        the voice and module against the available ones """

    arg_voice_type = setting.split('.', 2)[1]
    arg_voice_name, arg_module = raw_value.split('@', 2)

    if arg_voice_type == 'all':
        arg_voice_type = ALL_VOICE_TYPES
    else:
        arg_voice_type = [arg_voice_type]

    voices = _list_voices()
    dialect = ''
    for module, name, lang, variant in voices:
        if module == arg_module and name == arg_voice_name:
            break
    else:
        if arg_voice_name == 'default' and arg_module in (v[0] for v in voices):
            module = arg_module
            name = '%s default voice' % arg_module

            locale.setlocale(locale.LC_MESSAGES, '')

            messages_locale = locale.getlocale(locale.LC_MESSAGES)[0]
            if messages_locale is None or messages_locale == 'C':
                lang = 'en'
            else:
                lang, dialect = messages_locale.split('_', 2)
            variant = None
        else:
            return False, False, None, None, None

    profile = manager.getProfile()

    general = manager.getGeneralSettings(profile)
    # we only support the speechd server
    general['speechServerFactory'] = "orca.speechdispatcherfactory"
    general['speechServerInfo'] = [module, module]

    for voice_type in arg_voice_type:
        _dict_set_scoped(general, 'voices.%s.family.name' % voice_type, name)
        _dict_set_scoped(general, 'voices.%s.family.lang' % voice_type, lang)
        _dict_set_scoped(general, 'voices.%s.family.dialect' % voice_type, dialect)
        _dict_set_scoped(general, 'voices.%s.family.variant' % voice_type, variant)

    return (True, True, general, manager.getPronunciations(profile),
            manager.getKeybindings(profile))


def set_rate(setting, raw_value, manager):
    try:
        value = float(raw_value)
    except ValueError as ex:
        print("Failed to parse value '%s': %s" % (raw_value, ex))
        return False, False, None, None, None

    profile = manager.getProfile()

    general = manager.getGeneralSettings(profile)
    for voice_type in ALL_VOICE_TYPES:
        _dict_set_scoped(general, 'voices.%s.rate' % voice_type, value)

    return (True, True, general, manager.getPronunciations(profile),
            manager.getKeybindings(profile))


def get_rate(setting, manager, default=50):
    return get_generic_setting('voices.default.rate', manager, default=default)


def set_generic_setting(setting, raw_value, manager):
    # we parse the given value as JSON, and hope the caller didn't mess up the
    # value type.
    try:
        value = json.loads(raw_value)
    except json.JSONDecodeError as ex:
        print("Failed to parse JSON value '%s': %s" % (raw_value, ex))
        return False, False, None, None, None

    profile = manager.getProfile()

    general = manager.getGeneralSettings(profile)
    _dict_set_scoped(general, setting, value)

    return (True, True, general, manager.getPronunciations(profile),
            manager.getKeybindings(profile))


SPECIAL_CASES = {
    'startingProfile': {
        'get': get_starting_profile,
        'set': set_starting_profile,
    },
    # custom pseudo-settings that map to real ones
    '@voice.all': { 'get': get_voice, 'set': set_voice, },
    '@voice.default': { 'get': get_voice, 'set': set_voice, },
    '@voice.uppercase': { 'get': get_voice, 'set': set_voice, },
    '@voice.hyperlink': { 'get': get_voice, 'set': set_voice, },
    '@voice.system': { 'get': get_voice, 'set': set_voice, },
    '@voice.rate.all': { 'get': get_rate, 'set': set_rate, },
}


def get_setting(setting, default=None):
    if not HAVE_ORCA:
        return False

    manager = settings_manager.getManager()
    manager.activate()

    if setting in SPECIAL_CASES:
        handler = SPECIAL_CASES[setting]['get']
    else:
        handler = get_generic_setting

    return handler(setting, manager, default=default)


def restart_orca(only_if_running=True):
    """ Try and restart Orca if it is running.

    We try to spawn the executable as closely as possible as it was previously
    running, but if Orca is using setproctitle() we're out of luck, because
    its 'cmdline' and 'environment' have been overwritten with \0s.
    We try and somewhat detect that, but it has little effect apart preventing
    us from getting an useful info anyway.

    XXX: should we be using pgrep -u UID -x orca?  We re-implement the same,
         and it doesn't really help to spawn with the right arguments (as we'd
         need some escaping that it doesn't provide), but it is likely to be
         more portable, and if we then can't get the right arguments we alreay
         simple spawn `orca --replace`.
    """

    # read through /proc to find the Orca process and its properties
    # basically, it's something like pgrep -u UID -x orca -a
    uid = os.getuid()
    for pid_dir in os.listdir('/proc'):
        try:
            pid_path = os.path.join('/proc', pid_dir)
            info = os.lstat(pid_path)
            if info.st_uid != uid:
                continue
            with open(os.path.join(pid_path, 'comm')) as comm_fp:
                comm = comm_fp.read().rstrip()
                logging.debug('checking PID %s: %s', pid_dir, comm)
                if comm != 'orca':
                    continue
            # try and get the args and environment, which will not work in case
            # Orca used setproctitle()
            with open(os.path.join(pid_path, 'cmdline')) as cmdline_fp:
                args = cmdline_fp.read().split('\0')
                if args and not args[-1]:  # there should be a trailing \0
                    del args[-1]
                if args and args[0] == 'orca' and all(not arg for arg in args[1:]):
                    # looks like setproctitle() when through here, strip empty arguments
                    args = args[0:1]
                logging.debug('argv for PID %s: %r', pid_dir, args)

                # ignore this instance if it was started with some args
                skip_flags = ['-l', '--list-apps',
                              '-v', '--version',
                              '-h', '--help']
                if any(flag in args for flag in skip_flags):
                    continue
            with open(os.path.join(pid_path, 'environ')) as environ_fp:
                env = { pair[0]: pair[1] for pair in (pair.split('=', 2) for pair in environ_fp.read().split('\0') if pair) }
                if not env:
                    # and empty environment is suspicious and probably denotes
                    # setproctitle() usage -- meh.
                    env = None
                logging.debug('env for PID %s: %r', pid_dir, env)
            cwd = os.readlink(os.path.join(pid_path, 'cwd'))
            logging.debug('cwd for PID %s: %r', pid_dir, cwd)

            break
        except OSError as ex:
            logging.exception(ex)
            continue
    else:
        if only_if_running:
            logging.debug('No Orcas found, not restarting them')
            return

        args = ['orca']
        cwd = None
        env = None

    # strip args we don't want to restore
    args = [arg for arg in args if arg not in ['-s', '--setup']]
    # and add --replace if it's not already there
    if '--replace' not in args:
        args.append('--replace')

    logging.debug("spawning %r cwd=%r env=%r\n", args, cwd, env)
    subprocess.Popen(args, cwd=cwd, env=env)


def set_setting(setting, value):
    if not HAVE_ORCA:
        return False

    settingsManager = settings_manager.getManager()
    settingsManager.activate()
    profile = settingsManager.getProfile()

    if setting in SPECIAL_CASES:
        handler = SPECIAL_CASES[setting]['set']
    else:
        handler = set_generic_setting

    (success, save, general,
     pronunciations, keybindings) = handler(setting, value, settingsManager)

    if success and save:
        scriptManager = script_manager.getManager()
        settingsManager.saveSettings(scriptManager.getDefaultScript(),
                                     general, pronunciations, keybindings)

    # FIXME: how to get Orca to reload its settings?  I only know 2 solutions:
    # * restart Orca (which requires restarting the right one, and using the
    #   right options -- not 100% trivial)
    # * open Orca preferences (I think (?))
    # Here we try and restart Orca the best we can (which it doesn't help us
    # with), but it leads to announcing "screen reader on".
    restart_orca()

    return success


def _list_voices():
    locale.setlocale(locale.LC_MESSAGES, '')

    messages_locale = locale.getlocale(locale.LC_MESSAGES)[0]
    if messages_locale is None or messages_locale == 'C':
        locale_language = locale_lang = 'en'
    else:
        locale_lang, locale_dialect = messages_locale.split('_')
        locale_language = '-'.join([locale_lang, locale_dialect])

    voices = []
    client = speechd.SSIPClient(sys.argv[0])
    for module in client.list_output_modules():
        client.set_output_module(module)
        for name, lang, variant in client.list_synthesis_voices():
            if lang in (locale_lang, locale_language):
                voices.append((module, name, lang, variant))

    return voices



def list_voices():
    for module, name, lang, variant in _list_voices():
        print("{1}@{0}:{1} ({0})".format(module, name))

    return True


def list_profiles():
    if not HAVE_ORCA:
        return False

    manager = settings_manager.getManager()
    manager.activate()

    for name, nick in sorted(manager.availableProfiles(),
                             key=lambda n: n[0].casefold()):
        name = ' '.join([n.upper() if n in ['jaws', 'nvda'] else n
                         for n in name.split('_')])
        print('%s:%s' % (nick, name))

    return True


def usage(exit_code=1, full=False):
    print("USAGE: %s help|get|set|status" % sys.argv[0],
          file=sys.stdout if exit_code == 0 else sys.stderr)
    if full:
        print("""
Reads and writes Orca settings.

COMMANDS:

get SETTING [DEFAULT]   Gets the value of SETTING
list-profiles           Lists available profiles.
set SETTING VALUE       Sets the value of SETTING to VALUE, where VALUE is a
                        JSON representation of the value to set.  WARNING: no
                        type validation is performed on the value, which means
                        that it is possible to set e.g. a string to a boolean
                        value, which could potentially break Orca.
status [SETTING]        Checks whether SETTING can be written
help                    Show this help.""")
    sys.exit(exit_code)


def main():
    if len(sys.argv) < 2:
        usage()
    if sys.argv[1] == 'get' and len(sys.argv) in [3, 4]:
        default = sys.argv[3] if len(sys.argv) == 4 else None
        sys.exit(0 if get_setting(sys.argv[2], default=default) else 1)
    elif sys.argv[1] == 'set' and len(sys.argv) == 4:
        sys.exit(0 if set_setting(sys.argv[2], sys.argv[3]) else 1)
    elif sys.argv[1] == 'list-voices' and len(sys.argv) == 2:
        sys.exit(0 if list_voices() else 1)
    elif sys.argv[1] == 'list-profiles' and len(sys.argv) == 2:
        sys.exit(0 if list_profiles() else 1)
    elif sys.argv[1] == 'status':
        sys.exit(0 if HAVE_ORCA else 1)
    elif sys.argv[1] == 'help':
        usage(0, True)
    else:
        usage()


if __name__ == '__main__':
    main()

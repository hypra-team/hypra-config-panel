#!/usr/bin/env python3

import sys
import json
from fnmatch import fnmatchcase as fnmatch


def print_json_as_python_po(data, allowed_keys=None, domain=None):
    if isinstance(data, dict):
        for key, value in data.items():
            print_json_as_python_po(value, allowed_keys=allowed_keys, domain='.'.join((domain, key)) if domain else key)
    elif not any(fnmatch(domain, pat) for pat in allowed_keys):
        return
    elif isinstance(data, list):
        for value in data:
            print_json_as_python_po(value)
    elif isinstance(data, str):
        value = data.replace('\\', '\\\\')
        if not value:
            return
        if domain:
            print('gettext.pgettext("""%s""", """%s""")' % (domain, value))
        else:
            print('gettext.gettext("""%s""")' % value)


if __name__ == '__main__':
    for arg in sys.argv[1:]:
        with open(arg) as fp:
            data = json.load(fp)
            print_json_as_python_po(data, allowed_keys=['*.label',
                                                        'name', 'description',
                                                        'states.*', 'marks.*'])

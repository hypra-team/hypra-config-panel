#!/usr/bin/env python3
# -*- coding: utf-8; -*-
#
#  Copyright (C) 2021  Colomban Wendling <cwendling@hypra.fr>
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os


PACKAGE = 'hypra-config-panel'
GETTEXT_PACKAGE = PACKAGE

if 'HCP_PREFIX' in os.environ:
    PREFIX = os.environ['HCP_PREFIX']
elif 'HCP_DATADIR' in os.environ:
    PREFIX = os.path.join(os.environ['HCP_DATADIR'], '/..')
else:
    PREFIX = '/usr'
DATADIR = os.environ.get('HCP_DATADIR', os.path.join(PREFIX, 'share'))
LOCALEDIR = os.path.join(DATADIR, 'locale')
LIBEXECDIR = os.path.join(PREFIX, 'libexec')

PKGDATADIR = os.path.join(DATADIR, PACKAGE)
PKGLIBEXECDIR = os.path.join(LIBEXECDIR, PACKAGE)

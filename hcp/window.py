#!/usr/bin/env python3
# -*- coding: utf-8; -*-
#
#  Copyright (C) 2021  Colomban Wendling <cwendling@hypra.fr>
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os
import logging
import json
import gettext

import gi
gi.require_version("Gdk", "3.0")
gi.require_version("Gtk", "3.0")
from gi.repository import Gdk
from gi.repository import Gtk
from gi.repository import GLib

from hcp.config import DATADIR, PKGDATADIR
from hcp.module import Module
from hcp.widgets import Frame, StackSwitcherNotebook


class CategoriesData:
    def __init__(self, fp=None, path=None):
        if fp is None:
            with open(path, 'rb') as fp:
                self._data = json.load(fp)
        else:
            self._data = json.load(fp)

    def _get_category(self, cat_id):
        return self._data.get(cat_id, {})

    def _get_group(self, cat_id, group_id):
        return self._get_category(cat_id).get('groups', {}).get(group_id, {})

    @staticmethod
    def _get_value(node, key, fallback=None):
        value = node.get(key, fallback)
        if not isinstance(value, str) and value is not fallback:
            value = str(value)
        return value

    def get_category_value(self, cat_id, key, fallback=None):
        value = self._get_value(self._get_category(cat_id), key, fallback)
        if key == 'label':
            value = gettext.pgettext('.'.join([cat_id, key]), value)
        return value

    def get_group_value(self, cat_id, group_id, key, fallback=None):
        value = self._get_value(self._get_group(cat_id, group_id), key, fallback)
        if key == 'label':
            value = gettext.pgettext('.'.join([cat_id, 'groups', group_id, key]), value)
        return value


def find_widget(widget, gtype):
    if isinstance(widget, gtype):
        return widget
    elif isinstance(widget, Gtk.Container):
        for child in widget.get_children():
            found = find_widget(child, gtype)
            if found is not None:
                return found
    return None


@Gtk.Template.from_file(os.path.join(PKGDATADIR, 'window.ui'))
class Window(Gtk.ApplicationWindow):
    __gtype_name__ = 'HcpWindow'

    _content = Gtk.Template.Child('hcpContent')
    _switcher = Gtk.Template.Child('hcpTabList')

    @Gtk.Template.Callback()
    def _label_activate_link(self, label, uri):
        self._content.set_visible_child_name(uri)

    def get_options(self):
        """ returns a dict such as options[str(cat)][str(group)] = Option """
        options = {}

        # Import the plugins without polluting the namespace
        __import__('hcp.modules', None, None, ['*'], 0)
        for cls in Module.__subclasses__():
            logging.debug("Loading %s...", cls)
            try:
                mod = cls()
                mod_options = mod.get_options()
                logging.debug("%s provides %d options", cls, len(mod_options))
            except Exception as ex:
                logging.exception("Failed to load options from module %s", cls)
            else:
                for opt in mod_options:
                    if opt.category not in options:
                        options[opt.category] = {}
                    if opt.group not in options[opt.category]:
                        options[opt.category][opt.group] = []
                    options[opt.category][opt.group].append(opt)

        return options

    def __init__(self, application):
        super().__init__(application=application)
        self.maximize()

        StackSwitcherNotebook.patch(self._switcher)
        # hide the home tab from the list
        self._switcher.get_children()[0].set_visible(False)

        # Set icon path
        icon_path = os.path.join(DATADIR, 'icons')
        Gtk.IconTheme.get_for_screen(self.get_screen()).append_search_path(icon_path)

        # Load custom CSS
        try:
            css = Gtk.CssProvider()
            css.load_from_path(os.path.join(PKGDATADIR, 'window.css'))
            Gtk.StyleContext.add_provider_for_screen(self.get_screen(), css,
                                                     Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION)
        except GLib.Error as ex:
            logging.exception(ex)

        # set a CSS class when the homepage is shown
        def update_home_css_class(stack, pspec):
            cls = 'shows-home'
            if stack.get_visible_child_name() == 'home':
                self.get_style_context().add_class(cls)
                self.set_title(_("Welcome to the Hypra Config Panel!"))
            else:
                self.get_style_context().remove_class(cls)
                self.set_title(_("Hypra Config Panel"))
        update_home_css_class(self._content, None)
        self._content.connect('notify::visible-child', update_home_css_class)

        # Populate the UI with all plugins
        categories = CategoriesData(path=os.path.join(PKGDATADIR, 'categories.json'))

        label_size_group = Gtk.SizeGroup(Gtk.SizeGroupMode.HORIZONTAL)
        option_size_group = Gtk.SizeGroup(Gtk.SizeGroupMode.VERTICAL)
        for cat, groups in sorted(self.get_options().items(),
                                  key=lambda i: categories.get_category_value(i[0], 'sort-order', i[0])):
            tab = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=12,
                          visible=True, expand=True)
            for group, options in sorted(groups.items(),
                                         key=lambda i: categories.get_group_value(cat, i[0], 'sort-order', i[0])):
                frame = Frame(label=categories.get_group_value(cat, group, 'label', group),
                              icon_name=categories.get_group_value(cat, group, 'icon', None),
                              pixel_size=32, visible=True)
                tab.pack_start(frame, False, False, 0)
                box = Gtk.Box(orientation=Gtk.Orientation.VERTICAL,
                              spacing=6, visible=True, expand=False)
                frame.add(box)
                for option in options:
                    alignment = Gtk.Alignment(xalign=0, yalign=0.5, yscale=0,
                                              visible=True, expand=False,
                                              child=option)
                    option_size_group.add_widget(alignment)
                    box.pack_start(alignment, False, False, 0)
                    label = find_widget(option, Gtk.Label)
                    if label:
                        label_size_group.add_widget(label)
            self._content.add_titled(tab, name=cat, title=categories.get_category_value(cat, 'label', cat))

        self.connect('key-press-event', self.on_key_press_event)
        self.show()

    def on_key_press_event(self, widget, event):
        if event.keyval == Gdk.KEY_Escape:
            self.destroy()
            return True
        return False

#!/usr/bin/env python3
# -*- coding: utf-8; -*-
#
#  Copyright (C) 2021  Colomban Wendling <cwendling@hypra.fr>
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os
import fnmatch
import glob
import json
import logging
import subprocess
import enum
import shlex
import gettext

from functools import wraps
from concurrent.futures import ThreadPoolExecutor

import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk
from gi.repository import GObject
from gi.repository import GLib

from hcp.config import PKGDATADIR, PKGLIBEXECDIR
from hcp import module
from hcp.widgets import LabeledSwitch
from hcp.widgets.labeledswitch import LabelColon


logger = logging.getLogger('module-simple')


def trace(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        logger.debug('CALL: %s(%s)', func.__qualname__,
                     ', '.join([str(a) for a in args] + ['%r=%r' % (k, v) for k, v in kwargs.items()]))
        ret = func(*args, **kwargs)
        logger.debug('RET %r <= %s()', ret, func.__qualname__)
        return ret
    return wrapper


def noexcept(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        try:
            return func(*args, **kwargs)
        except Exception as ex:
            logger.exception(ex)
    return wrapper


class SettingStateEnum(enum.IntEnum):
    NONE = enum.auto()
    BUSY = NONE
    WRITE_ERROR = enum.auto()
    WRITE_OK = NONE
    READ_ERROR = enum.auto()
    READ_OK = NONE


class SettingState(Gtk.Stack):
    """Gtk widget representing info on the synchronization with a setting"""

    @GObject.Property(type=int, default=SettingStateEnum.NONE)
    def state(self):
        return self._state

    @state.setter
    def state(self, state):
        self._state = SettingStateEnum(state)
        self.set_visible_child_name(self._state.name)

    def __init__(self, state=SettingStateEnum.NONE, **kwargs):
        super().__init__(**kwargs)

        self._widgets = {
            SettingStateEnum.NONE:
                Gtk.Box(),  # nothing
            SettingStateEnum.WRITE_ERROR:
                Gtk.Image(icon_name='dialog-error',
                          icon_size=Gtk.IconSize.BUTTON,
                          tooltip_text=_('Failed to write setting')),
            SettingStateEnum.READ_ERROR:
                Gtk.Image(icon_name='dialog-error',
                          icon_size=Gtk.IconSize.BUTTON,
                          tooltip_text=_('Failed to load setting')),
        }

        for key, widget in self._widgets.items():
            self.add_named(widget, key.name)

        self.state = state


class SubprocessExecutor(ThreadPoolExecutor):
    def __init__(self, max_workers=None):
        if max_workers is None:
            # Python 3.8+ default
            max_workers = min(32, os.cpu_count() + 4)
        super().__init__(max_workers=max_workers,
                         thread_name_prefix="subprocess")

    def run(self, *args, callback=None, **kwargs):
        """ wrapper for subprocess.run() that works asynchronously.
        callback will be called with the CompleteProcess for this run.
        Returns a concurrent.futures.Future of the function actually running
        the subprocess. """

        # G_MESSAGES_DEBUG can lead some apps to spam stdout (especially the
        # gsettings binary), which is problematic when otherwise used to read
        # info.  As we use this envvar ourselves, filter it out when spawning
        # a child.
        if 'env' in kwargs and 'G_MESSAGES_DEBUG' in kwargs['env']:
            del kwargs['env']['G_MESSAGES_DEBUG']
        elif 'G_MESSAGES_DEBUG' in os.environ:
            kwargs['env'] = {k:v for k, v in os.environ.items() if k != 'G_MESSAGES_DEBUG'}

        @trace
        @noexcept
        def do_run(*args, callback=None, **kwargs):
            result = subprocess.run(*args, **kwargs)
            logger.debug('subprocess %s exited with code %s', result.args,
                         result.returncode)
            if result.stdout is not None:
                logger.debug('stdout=%s', result.stdout)
            if callback:
                GLib.idle_add(lambda: (False, callback(result))[0])

        return self.submit(do_run, *args, callback=callback, **kwargs)


class Option(Gtk.Box, module.Option):
    __gsignals__ = {
        'changed': (GObject.SIGNAL_RUN_FIRST, None, ())
    }

    _executor = SubprocessExecutor()
    _size_groups = {}

    def _run(self, *args, **kwargs):
        env = kwargs.pop('env') if 'env' in kwargs else os.environ.copy()
        env['PATH'] = os.pathsep.join([os.path.join(PKGLIBEXECDIR,
                                                    'modules/simple/helpers'),
                                       env.get('PATH', '')])
        return self._executor.run(*args, env=env, **kwargs)

    @trace
    def _get_value(self, callback):
        def on_child_exited(result):
            logger.debug(' -> code: %s, stdout="%s"', result.returncode,
                         result.stdout)

            self._readable = result.returncode == 0
            self._setting_state.state = (SettingStateEnum.READ_OK if self._readable
                                         else SettingStateEnum.READ_ERROR)
            self.set_sensitive(self._readable)

            if callback:
                raw_value = result.stdout.strip()
                logger.debug('looking for "%s" in %s', raw_value, self._states)
                if self._states is None:
                    value = raw_value
                elif raw_value in self._states:
                    value = self._states[raw_value]
                else:
                    value = None

                callback(self._readable, value, raw_value)

        logger.debug('Running command: %s', self._command_get)
        self._run(self._command_get, shell=True, text=True,
                  stdout=subprocess.PIPE, callback=on_child_exited)
        self._setting_state.state = SettingStateEnum.BUSY
        self.set_sensitive(False)

    @trace
    def _set_value(self, value, value_is_raw=False, callback=None):
        if not value_is_raw and self._states is not None:
            for raw_value, internal_value in self._states.items():
                if internal_value == value:
                    break
            else:
                raw_value = None
                return False
        else:
            raw_value = str(value)

        def process_queued_value():
            def on_child_exited(result):
                success = result.returncode == 0
                self._setting_state.state = (SettingStateEnum.WRITE_OK if success
                                             else SettingStateEnum.WRITE_ERROR)
                if callback:
                    callback(success)
                if success:
                    self.emit('changed')

                self._set_proc = None

                # run pending job if needed
                if self._set_queue:
                    process_queued_value()

            env = os.environ.copy()
            env['HCP_VALUE'] = self._set_queue.pop()
            logger.debug('Running command: %s', self._command_set)
            self._set_proc = self._run(self._command_set, shell=True,
                                       text=True, env=env,
                                       callback=on_child_exited)
            self._setting_state.state = SettingStateEnum.BUSY

        self._set_queue = [raw_value]  # replace the value
        if self._set_proc:
            # the suprocess dying will trigger processing of the queued value
            pass
        else:
            process_queued_value()

        return True

    def reload(self):
        pass

    @trace
    def set_sensitive(self, state=True):
        super().set_sensitive(self._enabled and self._readable and state)

    def do_states_loaded(self):
        pass

    @staticmethod
    def _translated_key(dct, key, default=None):
        value = dct.get(key, default)
        if value:
            value = gettext.pgettext(key, value)
        return value

    def __init__(self, json_data):
        super(Gtk.Box, self).__init__(visible=True, orientation=Gtk.Orientation.HORIZONTAL, spacing=6)
        super(module.Option, self).__init__()

        self._set_queued = []
        self._set_proc = None

        self._name = self._translated_key(json_data, 'name')
        self.category = json_data.get('category')
        self.group = json_data.get('group', None)
        self._states = json_data.get('states', None)
        self._enabled = True  # whether command-enabled succeeded
        self._readable = False  # whether command-get succeeded

        self.invalidates = json_data.get('invalidates', [])

        self._command_get = json_data['command-get']
        self._command_set = json_data['command-set']
        self._command_enabled = json_data.get('command-enabled', None)
        self._command_states = json_data.get('command-states', None)

        size_group_name = json_data.get('size-group', None)
        if size_group_name is not None:
            if size_group_name not in self._size_groups:
                self._size_groups[size_group_name] = Gtk.SizeGroup(Gtk.SizeGroupMode.HORIZONTAL)
            self._size_group = self._size_groups[size_group_name]
        else:
            self._size_group = None

        description = self._translated_key(json_data, 'description', None)
        if description:
            self.set_tooltip_text(description)

        if self._command_enabled:
            self._enabled = False
            self.set_sensitive(False)

            def on_command_enabled_exited(result):
                self._enabled = result.returncode == 0
                self.set_sensitive(self._enabled)
                if result.stdout:
                    logger.warning("Command %r wrote to stdout, but only the "
                                   "exit status is used for command-enabled. "
                                   "To silence this warning, do not write to "
                                   "stdout from this command. "
                                   "stdout was:\n--- 8< ---\n%s\n--- 8< ---",
                                   result.args, result.stdout)

            self._run(self._command_enabled, shell=True,
                      stdout=subprocess.PIPE, text=True,
                      callback=on_command_enabled_exited)

        if self._states and self.do_states_loaded is not Option.do_states_loaded:
            GLib.idle_add(lambda: (False, self.do_states_loaded())[0])
        if self._command_states:
            def on_command_states_exited(result):
                if result.returncode == 0:
                    if self._states is None:
                        self._states = {}
                    for line in result.stdout.splitlines():
                        logger.debug("parsing state line '%s'" % (line))
                        state, label = (i.strip() for i in line.split(':', 2))
                        logger.debug("Got state='%s' label='%s'" % (state, label))
                        self._states[state] = label
                    self.do_states_loaded()

            self._run(self._command_states, shell=True,
                      stdout=subprocess.PIPE, text=True,
                      callback=on_command_states_exited)

        self._setting_state = SettingState(visible=True)
        self.pack_end(self._setting_state, False, True, 0)


class BoolSwitchOption(Option):
    def _state_set(self, switch, state):
        def on_result(success):
            with switch.handler_block(self._state_set_handler):
                if success:
                    self._switch.state = state
                else:
                    self._switch.active = self._switch.state

        self._set_value(state, callback=on_result)
        return True

    def __init__(self, json_data):
        super().__init__(json_data=json_data)

        self._switch = LabeledSwitch(label=self._name, expand=True, visible=True)
        self._state_set_handler = self._switch.connect('state-set', self._state_set)
        self.add(self._switch)

        self.reload()

    def reload(self):
        def on_value(success, value, raw_value=None):
            if success:
                with self._switch.handler_block(self._state_set_handler):
                    self._switch.state = value

        self._get_value(on_value)


class BoolCheckOption(Option):
    def _notify_active(self, check, pspec):
        self._set_value(check.get_active())

    def __init__(self, json_data):
        super().__init__(json_data=json_data)

        self._check = Gtk.CheckButton(label=self._name, expand=True, visible=True)
        self._notify_active_handler = self._check.connect('notify::active', self._notify_active)
        self.add(self._check)

        self.reload()

    def reload(self):
        def on_value(success, value, raw_value=None):
            if success:
                with self._check.handler_block(self._notify_active_handler):
                    self._check.set_active(value)

        self._get_value(on_value)


class ComboOption(Option):
    def _changed(self, combo):
        self._set_value(combo.get_active_id(), value_is_raw=True)

    def _fill_options(self):
        for raw, pretty in self._states.items():
            self._combo.append(str(raw),
                               gettext.pgettext('.'.join(('states', raw)),
                                                str(pretty)))

        # Remove the dummy item now we loaded values
        if self._has_empty_item:
            with self._combo.handler_block(self._changed_handler):
                self._combo.remove(0)
                self._has_empty_item = False

    def do_states_loaded(self):
        self._fill_options()
        self.reload()

    def reload(self):
        # update the value once the states have been loaded
        def on_value(success, value, raw_value):
            if success:
                with self._combo.handler_block(self._changed_handler):
                    if not self._combo.set_active_id(str(raw_value)):
                        self._combo.set_active_id(None)
        self._get_value(on_value)

    def __init__(self, json_data):
        super().__init__(json_data=json_data)

        self._box = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=6,
                            expand=True, visible=True)
        self._combo = Gtk.ComboBoxText(visible=True)
        # Add a dummy item for the combo box not to change height on us when
        # getting filled later on
        self._combo.insert(0, None, _('Loading...'))
        self._has_empty_item = True
        self._combo.set_active(0)
        self._label = LabelColon(label=self._name, xalign=0,
                                 mnemonic_widget=self._combo,
                                 visible=True)
        if self._size_group:
            self._size_group.add_widget(self._label)

        self._box.pack_start(self._label, True, True, 0)
        self._box.pack_start(self._combo, False, False, 0)
        self.add(self._box)

        self._changed_handler = self._combo.connect('changed', self._changed)


class BoolComboOption(ComboOption):
    def _fill_options(self):
        added_values = []
        for raw, internal in self._states.items():
            if internal not in added_values:
                self._combo.append(raw, "Enabled" if internal else "Disabled")
                added_values.append(internal)


class BoolOption(BoolSwitchOption):
    pass


class RangeOption(Option):
    def _changed(self, adjustment):
        value = adjustment.get_value()
        if self._precision == 0:
            value = int(value)
        else:
            value = round(value, self._precision)
        self._set_value(value)

    def __init__(self, json_data):
        super().__init__(json_data=json_data)

        self._command_range = json_data.get('command-range', None)
        if self._command_range:
            self._range = { 'min': 0, 'max': 0 }
        else:
            self._range = json_data['range']
        self._precision = int(json_data.get('precision', '0'))

        self._adjustment = Gtk.Adjustment(value=float(self._range['min']),
                                          lower=float(self._range['min']),
                                          upper=float(self._range['max']),
                                          step_increment=1.0 / max(1, 10 * self._precision),
                                          page_increment=10.0 / max(1, 10 * self._precision),
                                          page_size=0)
        self._changed_handler = self._adjustment.connect('value-changed', self._changed)

        self.reload()

    def _reload_range(self):
        def on_command_range_exited(result):
            if result.returncode == 0:
                self._range['min'], self._range['max'] = \
                    (float(v) for v in result.stdout.split(':'))
                logger.debug('Got range change to %s:%s', *self._range.values())
                self._adjustment.set_lower(self._range['min'])
                self._adjustment.set_upper(self._range['max'])

                self._reload_value()
            else:
                self._readable = False
                self.set_sensitive(False)

        self._run(self._command_range, shell=True,
                  stdout=subprocess.PIPE, text=True,
                  callback=on_command_range_exited)

    def _reload_value(self):
        def on_value(success, value, raw_value):
            if success:
                with self._adjustment.handler_block(self._changed_handler):
                    self._adjustment.set_value(float(value))
        self._get_value(on_value)

    def reload(self):
        if self._command_range:
            self._reload_range()  # chains to reloading the value
        else:
            self._reload_value()


class ScaleOption(RangeOption):
    def _update_marks(self):
        self._scale.clear_marks()
        for value, mark in self._marks.items():
            if value == 'min':
                actual_value = self._adjustment.get_lower()
            elif value == 'max':
                actual_value = self._adjustment.get_upper()
            else:
                actual_value = float(value)
            self._scale.add_mark(actual_value, Gtk.PositionType.BOTTOM,
                                 gettext.pgettext('marks.%s' % value, mark))

    def _reload_value(self):
        super()._reload_value()
        # if we have a command for the range, we get called after the range
        # has been reloaded, andthus can update the marks
        if self._command_range:
            self._update_marks()

    def __init__(self, json_data):
        super().__init__(json_data=json_data)

        value_position = json_data.get('value-position', 'left')
        draw_value = value_position is not None
        value_pos = {
            None: Gtk.PositionType.LEFT,
            'left': Gtk.PositionType.LEFT,
            'right': Gtk.PositionType.RIGHT,
            'top': Gtk.PositionType.TOP,
            'bottom': Gtk.PositionType.BOTTOM,
        }[value_position]

        self._scale = Gtk.Scale(adjustment=self._adjustment, digits=self._precision,
                                value_pos=value_pos, draw_value=draw_value,
                                round_digits=self._precision, visible=True)
        self._label = LabelColon(label=self._name, xalign=0,
                                 mnemonic_widget=self._scale, visible=True)
        if self._size_group:
            self._size_group.add_widget(self._label)

        self._marks = json_data.get('marks', {})
        self._update_marks()

        self.pack_start(self._label, False, True, 0)
        self.pack_start(self._scale, True, True, 0)


class SpinOption(RangeOption):
    def __init__(self, json_data):
        super().__init__(json_data=json_data)

        self._spin = Gtk.SpinButton(adjustment=self._adjustment,
                                    digits=self._precision, visible=True)
        self._label = LabelColon(label=self._name, xalign=0,
                                 mnemonic_widget=self._spin, visible=True)
        if self._size_group:
            self._size_group.add_widget(self._label)

        self.pack_start(self._label, True, True, 0)
        self.pack_start(self._spin, False, True, 0)


class Simple(module.Module):
    def __init__(self):
        self._options = None

    def _option_changed(self, changed_option):
        logger.debug('option %s changed', changed_option.id)
        for pattern in changed_option.invalidates:
            for option in self._options:
                if changed_option == option:
                    continue
                if fnmatch.fnmatch(option.id, pattern):
                    logger.debug('this change invalidated option %s', option.id)
                    option.reload()

    def get_options(self):
        if self._options is not None:
            return self._options

        self._options = []

        for options_file in sorted(glob.glob(os.path.join(PKGDATADIR, 'modules/simple/options/*.json'))):
            logger.debug('loading JSON options file "%s"', options_file)
            try:
                with open(options_file) as fp:
                    json_data = json.load(fp)
            except Exception as ex:
                logger.exception('failed to load JSON file "%s"', options_file)
            else:
                logger.debug(json_data)
                types = {
                    'boolean': BoolOption,
                    'boolean-switch': BoolSwitchOption,
                    'boolean-combo': BoolComboOption,
                    'boolean-check': BoolCheckOption,
                    'combo': ComboOption,
                    'scale': ScaleOption,
                    'spin': SpinOption,
                }
                if 'type' not in json_data or json_data['type'] not in types:
                    logger.warning('"%s": missing or invalid "type" key', options_file)
                    continue
                try:
                    option = types[json_data['type']](json_data)
                except Exception as ex:
                    logger.exception('failed to create option from %s', options_file)
                else:
                    option.id = os.path.basename(options_file)
                    option.connect('changed', self._option_changed)
                    self._options.append(option)

        return self._options

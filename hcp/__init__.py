#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
import gettext
import locale

from .config import GETTEXT_PACKAGE, LOCALEDIR


# setup i18n
locale.setlocale(locale.LC_ALL, '')
gettext.install(GETTEXT_PACKAGE, LOCALEDIR)
gettext.bindtextdomain(GETTEXT_PACKAGE, LOCALEDIR)
gettext.textdomain(GETTEXT_PACKAGE)
locale.bindtextdomain(GETTEXT_PACKAGE, LOCALEDIR)
locale.textdomain(GETTEXT_PACKAGE)

# setup logging
try:
    debug_domains = os.environ['G_MESSAGES_DEBUG']
except KeyError:
    pass
else:
    if debug_domains == 'all' or GETTEXT_PACKAGE in debug_domains.split(' '):
        import logging

        logging.basicConfig(level=logging.DEBUG)


# provide a fallback implementation for gettext.pgettext in Python < 3.8
if 'pgettext' not in gettext.__all__:
    import logging
    def _pgettext(ctx, msg):
        logging.debug('Looking up string "%s" from context "%s"', msg, ctx)
        lookup = '\x04'.join((ctx, msg))
        result = gettext.gettext(lookup)
        if result == lookup:
            return msg
        return result
    gettext.pgettext = _pgettext

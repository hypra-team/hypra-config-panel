#!/usr/bin/env python3
# -*- coding: utf-8; -*-
#
#  Copyright (C) 2021  Colomban Wendling <cwendling@hypra.fr>
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.

import gi
from gi.repository import Gio
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk

from .window import Window


class Application(Gtk.Application):
    def __init__(self):
        super().__init__(application_id="fr.hypra.hcp",
                         flags=Gio.ApplicationFlags.FLAGS_NONE)

    def do_activate(self, *args):
        window = self.get_active_window()
        if not window:
            window = Window(self)
        window.present()


def main():
    app = Application()
    return app.run()

#!/usr/bin/env python3
# -*- coding: utf-8; -*-
#
#  Copyright (C) 2021  Colomban Wendling <cwendling@hypra.fr>
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.

import gi
gi.require_version("Gdk", "3.0")
gi.require_version("Gtk", "3.0")
from gi.repository import Gdk
from gi.repository import Gtk
from gi.repository import GLib


# Taken from GTK's gtkprivate.c
def _gtk_translate_keyboard_accel_state(keymap : Gdk.Keymap,
                                        hardware_keycode : int,
                                        state : Gdk.ModifierType,
                                        accel_mask : Gdk.ModifierType,
                                        group : int):
    shift_group_mask : Gdk.ModifierType
    group_mask_disabled : bool = False

    # if the group-toggling modifier is part of the accel mod mask, and
    # it is active, disable it for matching
    shift_group_mask = keymap.get_modifier_mask(Gdk.ModifierIntent.SHIFT_GROUP)
    if accel_mask & state & shift_group_mask:
        state &= ~shift_group_mask
        group = 0
        group_mask_disabled = True

    success, keyval, effective_group, level, consumed_modifiers = \
            keymap.translate_keyboard_state(hardware_keycode, Gdk.ModifierType(state), group)

    # add back the group mask, we want to match against the modifier,
    # but not against the keyval from its group
    if group_mask_disabled:
        effective_group = 1
        consumed_modifiers &= ~shift_group_mask

    return success, keyval, effective_group, level, consumed_modifiers


def _translate_key_event(widget, event):
    keymap = Gdk.Keymap.get_for_display(widget.get_display())
    accel_mask = Gtk.accelerator_get_default_mod_mask()
    success, keyval, group, level, consumed = \
        _gtk_translate_keyboard_accel_state(keymap, event.hardware_keycode,
                                            event.state, accel_mask,
                                            event.group)
    if not success:
        return (False, 0, 0)

    return True, event.state & ~consumed & accel_mask, keyval


class _patch_stack_switcher:
    """ Patches a GtkStackSwitcher to behave more like a GtkNotebook for key
    navigation.  This notably adds handling of the arrow keys on the buttons
    to switch page and keeping the focus on the selected button, avoiding focus
    on non-current page, and <Primary>PageUp/Down to switch pages both on the
    switcher and the controlled stack.

    Usage:
    >>> switcher = Gtk.StackSwitcher()
    >>>> patch_stack_switcher(switcher)

    This is a convenient way to boost an existing stack switcher (e.g. from a
    Gtk.Builder definition), but it relies on some implementation details of
    the stack switcher (see inline comments).

    A more standard class is provided, StackSwitcherNotebook, which simply is
    a Gtk.StackSwitcher with this patching applied.

    One possible accessibility issue is that navigating through the switcher
    reports radio buttons, which does not completely convey the purpose of the
    control.  Interaction is fortunately very similar between radio buttons and
    notebook pages, so navigating should be fairly natural still.  We can't
    simply alter the accessible role to make it a page tab, because the
    buttons send signals (like "checked") that are interpreted by the screen
    reader, and lead to noisy results like announcing the selected page tab
    twice. """

    def __init__(self, switcher, cycle=True):
        self._switcher = switcher
        self._cycle = cycle
        self._stack = None

        self._stack_changed()

        self._switcher.connect('notify::stack', lambda o, p: self._stack_changed())
        self._switcher.connect('key-press-event', self._on_switcher_key_press_event)

    def _on_stack_destroy(self, stack):
        """ Apparently Gtk.StackSwitcher is not very well behaved if its stack
        gets destroyed, so as this affects us, improve handling """
        if stack == self._stack:
            self._switcher.set_stack(None)

    def _on_stack_key_press_event(self, stack, event):
        success, state, keyval = _translate_key_event(stack, event)
        if not success:
            return False

        forward_keys = (Gdk.KEY_Page_Down, Gdk.KEY_Tab)
        backward_keys = (Gdk.KEY_Page_Up, Gdk.KEY_ISO_Left_Tab)

        if keyval in forward_keys + backward_keys and \
           state == Gdk.ModifierType.CONTROL_MASK:
            return self._switch_page(1 if keyval in forward_keys else -1)
        return False

    def _on_stack_add(self, stack, child):
        self._stack_notify_visible_child(stack)

    def _stack_notify_visible_child(self, stack, pspec=None):
        children = stack.get_children()
        visible_child = stack.get_visible_child()
        if not visible_child:  # the stack is likely not yet populated
            return

        # FIXME: this relies on Gtk.StackSwitcher internals, namely the mapping
        # from stack children to switcher widgets.
        nth_child = children.index(visible_child)
        tabs = self._switcher.get_children()
        had_focus = False
        for i, tab in enumerate(tabs):
            had_focus |= tab.is_focus()
            tab.set_can_focus(i == nth_child)
        tab = tabs[nth_child]
        if not tab.get_visible():
            # find the first visible tab ideally after the expected one
            tab = next((t for i, t in sorted(((i, t) for i, t in enumerate(tabs) if t.get_visible()),
                                             key=lambda x: x[0] <= nth_child)), None)
        if tab is not None:
            tab.set_can_focus(True)
            if had_focus:
                tab.grab_focus()

    def _stack_changed(self):
        if self._stack:
            self._stack.disconnect_by_func(self._stack_notify_visible_child)
            self._stack.disconnect_by_func(self._on_stack_key_press_event)
            self._stack.disconnect_by_func(self._on_stack_destroy)
            self._stack.disconnect_by_func(self._on_stack_add)
        self._stack = self._switcher.get_stack()
        if self._stack:
            self._stack_notify_visible_child(self._stack)
            self._stack.connect('destroy', self._on_stack_destroy)
            self._stack.connect('notify::visible-child',
                                self._stack_notify_visible_child)
            self._stack.connect_after('add', self._on_stack_add)
            self._stack.connect('key-press-event',
                                self._on_stack_key_press_event)

    def _switch_page(self, step):
        children = [c for c in self._stack.get_children() if c.is_visible()]
        visible_child = self._stack.get_visible_child()
        if not visible_child:
            return False

        nth_child = children.index(visible_child)
        tabs = self._switcher.get_children()
        # loop in case of hidden tabs, but no more than a whole revolution
        for i in range(len(tabs)):
            nth_child += step

            if self._cycle:
                nth_child %= len(children)
            elif not 0 <= nth_child < len(children):
                return False

            if tabs[nth_child].get_visible():
                break

        self._stack.set_visible_child(children[nth_child])

        # Handle the odd case where focus was inside the tab we switched from
        # and the one we switched to has nothing to focus, in which case GTK is
        # just losing all focus.  In such a case, move the focus back on the
        # relevant tab (the only focusable one) so navigation continues.
        def _ensure_focus():
            focused = self._switcher.get_toplevel().get_focus()
            if focused is None:
                def focus_first_child(widget):
                    if widget.get_can_focus():
                        widget.grab_focus()
                        return True
                    if isinstance(widget, Gtk.Container):
                        for chld in widget:
                            if focus_first_child(chld):
                                return True
                    return False

                focus_first_child(self._switcher)

            return False

        GLib.idle_add(_ensure_focus)

        return True

    def _on_switcher_key_press_event(self, switcher, event):
        success, state, keyval = _translate_key_event(switcher, event)
        if not success:
            return False

        if state == 0:
            if switcher.get_orientation() == Gtk.Orientation.VERTICAL:
                if keyval == Gdk.KEY_Up:
                    return self._switch_page(-1)
                if keyval == Gdk.KEY_Down:
                    return self._switch_page(1)
            else:
                if keyval == Gdk.KEY_Left:
                    return self._switch_page(-1)
                if keyval == Gdk.KEY_Right:
                    return self._switch_page(1)
        if self._stack:
            return self._on_stack_key_press_event(self._stack, event)
        return False


class StackSwitcherNotebook(Gtk.StackSwitcher):
    """ A Gtk.StackSwitcher with enhanced keyboard navigation """

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.patch(self)

    @staticmethod
    def patch(switcher: Gtk.StackSwitcher):
        """ Patches a Gtk.StackSwitcher to gain enhanced features from this class """
        _patch_stack_switcher(switcher)
        return switcher


if __name__ == '__main__':
    def main():
        """ Example app to test behavior and difference with similar widgets """
        win = Gtk.Window(border_width=12)
        win.connect('destroy', Gtk.main_quit)
        grid = Gtk.Grid()
        win.add(grid)

        stack = Gtk.Stack()
        grid.attach(stack, 0, 1, 1, 1)
        for i in range(5):
            page = Gtk.Grid()
            page.attach(Gtk.Label(label="Page %s" % (i + 1)), 0, 0, 1, 1)
            page.attach(Gtk.Button(label="Button %s" % (i + 1)), 0, 1, 1, 1)
            stack.add_titled(page, name="page%s" % (i + 1), title="Page %s" % (i + 1))

        tabs = StackSwitcherNotebook(stack=stack)
        grid.attach(tabs, 0, 0, 1, 1)
        tabs2 = StackSwitcherNotebook(stack=stack, orientation=Gtk.Orientation.VERTICAL)
        grid.attach(tabs2, 1, 0, 1, 2)

        notebook = Gtk.Notebook(tab_pos=Gtk.PositionType.LEFT)
        grid.attach(notebook, 0, 2, 2, 1)
        for i in range(3):
            page = Gtk.Grid()
            page.attach(Gtk.Label(label="Page %s" % (i + 1)), 0, 0, 1, 1)
            page.attach(Gtk.Button(label="Button %s" % (i + 1)), 0, 1, 1, 1)
            notebook.append_page(page, tab_label=Gtk.Label(label="Page %s" % (i + 1)))

        box = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL)
        grid.attach(box, 0, 3, 2, 1)
        group = None
        for i in range(3):
            button = Gtk.RadioButton(group=group, label="Radio button %s" % (i + 1))
            if not group:
                group = button
            box.pack_start(button, True, False, 0)

        win.show_all()
        stack.get_children()[1].hide()
        Gtk.main()

    main()

#!/usr/bin/env python3
# -*- coding: utf-8; -*-
#
#  Copyright (C) 2020  Colomban Wendling <cwendling@hypra.fr>
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.

import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk
from gi.repository import GObject


class LabelColon(Gtk.Label):
    """ A label that has a colon after it, but not in its accessible name.
    This is useful for UIs where a colon makes sense visually but is solely
    labeling another widget pointed to by the colon.  This allows to have a
    colon-less accessible name for the target widget, yet have the colon
    visible in the UI. """

    def __init__(self, label, use_underline=False, **props):
        super().__init__(label=_("%s:") % label, use_underline=use_underline,
                         **props)
        if use_underline:
            a11y_label = label.replace('_', '', 1)
        else:
            a11y_label = label
        self.get_accessible().set_name(a11y_label)


class LabeledSwitch(Gtk.Box):
    """ A Gtk.Switch that has a label, kind of like a Gtk.Checkbox """

    __gsignals__ = {
        'activate': (GObject.SIGNAL_RUN_FIRST | GObject.SIGNAL_ACTION, None, ()),
        'state-set': (GObject.SIGNAL_RUN_LAST, bool, (bool, )),
    }

    @GObject.Property(type=bool, default=False)
    def active(self):
        return self._switch.get_active()

    @active.setter
    def active(self, active):
        self._switch.set_active(active)

    @GObject.Property(type=bool, default=False)
    def state(self):
        return self._switch.get_state()

    @state.setter
    def state(self, state):
        self._switch.set_state(state)

    def __init__(self, active=False, state=False,
                 label=None, use_underline=False,
                 tooltip_text=None, **props):
        props['orientation'] = props.pop('orientation', Gtk.Orientation.HORIZONTAL)
        props['spacing'] = props.pop('spacing', 6)

        super().__init__(**props)

        self._switch = Gtk.Switch(active=active, state=state,
                                  tooltip_text=tooltip_text,
                                  visible=True)
        self._switch.connect('notify::active', lambda o, p: self.notify(p.name))
        self._switch.connect('notify::state', lambda o, p: self.notify(p.name))
        self._switch.connect('state-set', lambda o, s: self.emit('state-set', s))
        self.connect('activate', lambda o: self._switch.emit('activate'))

        self._label = LabelColon(label=label, tooltip_text=tooltip_text,
                                 use_underline=use_underline,
                                 mnemonic_widget=self._switch,
                                 visible=True)
        if props['orientation'] == Gtk.Orientation.HORIZONTAL:
            self._label.set_xalign(0)
        else:
            self._label.set_yalign(0)

        self.pack_start(self._label, True, True, 0)
        self.pack_start(self._switch, False, False, 0)

#!/usr/bin/env python3
# -*- coding: utf-8; -*-
#
#  Copyright (C) 2020  Colomban Wendling <cwendling@hypra.fr>
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.

import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk
from gi.repository import Pango
from gi.repository import GLib


class Frame(Gtk.Frame):
    """ A Glade-style frame """

    def __init__(self, icon_name=None, icon_size=Gtk.IconSize.BUTTON,
                 pixel_size=-1, **props):
        if 'shadow_type' not in props:
            props['shadow_type'] = Gtk.ShadowType.NONE

        super().__init__(**props)

        if 'label_widget' not in props:
            label = self.get_label_widget()
            if hasattr(Pango, 'attr_weight_new'):
                attr_list = Pango.AttrList()
                attr_list.insert(Pango.attr_weight_new(Pango.Weight.BOLD))
                label.set_attributes(attr_list)
            else:
                label_text = label.get_label()
                if not label.get_use_markup():
                    label_text = GLib.markup_escape_text(label_text)
                label.set_label('<b>' + label_text + '</b>')
                label.set_use_markup(True)

            if icon_name is not None:
                box = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL,
                              spacing=12, visible=True, expand=False)
                box.pack_start(Gtk.Image(icon_name=icon_name,
                                         icon_size=icon_size,
                                         pixel_size=pixel_size,
                                         visible=True),
                               False, False, 0)
                label.get_parent().remove(label)
                box.pack_start(label, True, True, 0)
                self.set_label_widget(box)
                # Set accessible name as GTK doesn't do that itself when the
                # label widget isn't a label
                self.get_accessible().set_name(label.get_text())

    def add(self, child):
        super().add(child)
        if child.get_margin_start() == 0:
            child.set_margin_start(12)

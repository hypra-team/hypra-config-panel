Hypra Config Panel
Copyright (C) 2021  Colomban Wendling <cwendling@hypra.fr>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

1. Usage

The Hypra Config Panel may be launched either from the provided
desktop launcher, or through the hypra-config-panel command.
No additional arguments are supported at this stage.

2. Tabs

The Hypra Config Panel is comprised of a Home tab devoid of any
interactive element, and of three more tabs:
- Mouse
- Visual
- Vocal
Navigation through these tabs may be achieved either by clicking
their buttons, or using the keyboard with Ctrl-Tab or Ctrl-PageUp.
When a tab button is focused, Left/Right arrows are also an option.
If an element in a given tab has the focus, that focus is retained
when switching to another tab and back again.

2.1 Mouse

There are three subcategories in this tab.
Interactive elements may be navigated with the Tab key;
labels are correctly vocalized for each element.
Tooltips are available on hover, but not vocalized.

2.1.1 General

“Pointer localization at startup” (switch): display or hide
vertical and horizontal lines emphasizing the pointer’s posision.
This is achieved internally through the “showmouse” Compiz
plugin (if not yet loaded, that plugin will first be loaded on
switching to “enabled”). If enabled, it gets enabled right
away as well as at the next session start; if disabled, it will
be disabled at the next session start but does not get disabled
right away (you need to press <Super>K for that).

“Click type”: select either “Simple” or “Double” to determine
whether files and folders are opened with a simple or double-click;
this is achieved internally by setting the “click-policy” property
of the “org.mate.caja.preferences” object.

“Pointer color” (several colors available): sets the mouse
pointer’s color through the ComixCursors cursor theme if
available; otherwise, displays a list of available cursor themes.
This is achieved internally by setting the “cursor-theme” property
of the “org.mate.peripherals-mouse” object.

“Pointer size” (range): defines the cursor’s size if supported,
through the “cursor-size” property of org.mate.peripherals-mouse.
Note that the “default” pointer theme does not support cursor
scalability.

2.1.2 Touchpad

“Use touchpad” (switch): enables or disables the touchpad input
device. This is achieved internally by setting the “touchpad-enabled”
property of the “org.mate.peripherals-touchpad” object; this setting
is applied right away and does persist after session restart.

“Touchpad speed” (range): sets the touchpad’s pointer acceleration.
This is achieved internally by setting the “motion-acceleration”
property of the “org.mate.peripherals-touchpad” object (values may
vary between 0 and 10, though they tend to be non-linear and more
fine-grained below 5). Default is 5.5.

2.1.3 Mouse

“Mouse speed” (range): sets the mouse’s pointer acceleration.
This is achieved internally by setting the “motion-acceleration” 
property of the “org.mate.peripherals-mouse” object (values may 
vary between 0 and 10, though they tend to be non-linear and more 
fine-grained below 5). Default is 5.5.

2.2 Visual

There are three subcategories in this tab.
Interactive elements may be navigated with the Tab key;   
labels are correctly vocalized for each element.  
Tooltips are available on hover, but not vocalized. 

2.2.1 Zoom

“Zoom level” (range): enables the “ezoom” Compiz plugin
if deactivated, and sets not only the current zoom level
but also the zoom level at session start.

2.2.2 Theme

“Color inversion at startup” (switch): enables or disables the
“neg” Compiz plugin for the entire screen. If enabled, it gets
enabled right away as well as at the next session start; if
disabled, it will be disabled at the next session start but
does not get disabled right away (you need to press <Super>M
for that).

2.2.3 Font

“Font” (list): select the globally-used desktop font from an
arbitrary selection of fonts (without checking for their availability
on the system). This sets four properties internally:
“font-name” in “org.mate.interface”,
“font” in “org.mate.caja.desktop”
“document-font-name” in “org.mate.interface” and
“titlebar-font” in “org.mate.Marco.general”.

2.3 Vocal

There are two subcategories in this tab.
Interactive elements may be navigated with the Tab key;
labels are correctly vocalized for each element.
Tooltips are available on hover, but not vocalized.

2.3.1 General

“Use the screen reader” (switch): report whether the Orca
screen reader is currently enabled (without testing for autostart
status). If enabled, sets the enableSpeech Orca property, and
makes sure an Orca executable is included in the relevant
autostart XDG config directory. Braille output is not handled.

2.3.2 Screen Reader

Setting any of the following items causes Orca to
restart, thus issuing the “screen reader on” vocal
message.

“Profile” (list): list available Orca profiles and allow to
redefine the startingProfile Orca property accordingly.
Switching profiles loses any custom overrides
to the current profile.

“Spell keyboard keys” (switch): speak key names as
they’re pressed. This is achieved by setting the
enableKeyEcho Orca property.

“Mouse review” (switch): speak object names on pointer hover.
This is achieved by setting the enableMouseReview Orca
property.

“Speech rate” (list): set the speech rate through all available
voices to a given value. Four predefined rates are provided.

“Voice” (list): sift through a list of available voices,
ordered by speech synthesis system.

3. Live updates

Upon launch, the Hypra Config Panel looks for updated definition,
proprerty and helper files. This is achieved by sending an HTTPS
request to https://nextcloud.hypra.fr/s/5DLgpH92eTtgT6E/download.
If available, these files are downloaded in a user-specific cache
directory and used right away without any need to restart the
application, unless download takes longer than 2 seconds. If no
updated files are found, no online access is currently available,
or the new version is corrupted, the latest working cached version
is used (or the system-wide installed version upon first launch
or as a last resort).

4. Look-and-feel

There are no “Apply” or “Validate” buttons; any property change
is taken into account immediately (and the relevant widget, e.g.
switch, is then updated accordingly).

Hitting the Escape key at any point closes the application
properly (without any error return code).

There is no “About” dialog, menu bar nor any additional information
to be found anywhere.

The Hypra Config Panel is unsuitable for a horizontal screen resolution
of less than 800 pixels, nor for a vertical resolution of less than
768 pixels (problems start to appear when the global font size exceeds
a value of 16). There are no scrollbars available, for better or worse.

Application metadata is not available so far.
